﻿using UnityEngine;
using System.Collections;

public class spiderController : MonoBehaviour {

	public GameObject spider;
	public GameObject player;
	public int delay;

	private int timer;

	void Start () {
		Random.seed = (int)(transform.position.x*100);
		int n = Random.Range (0, 50);
		timer = 100 + delay + n;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (timer > 90) {
			timer--;
		} else if (timer == 90) {
			spider.SetActive (true);
			spider.transform.position = transform.position;
			timer--;
		} else if (timer > 0) {
			spider.transform.position = transform.position;
			timer--;
		} else if (timer == 0) {
			float vX = player.transform.position.x - spider.transform.position.x;
			float vY = player.transform.position.y - spider.transform.position.y;
			Vector2 movement = new Vector2 (vX, vY);
			movement.Normalize ();
			spider.GetComponent<Rigidbody2D> ().velocity = movement*2;
			timer--;
		} else if (!spider.activeSelf){
			Random.seed = (int)(transform.position.x*100);
			int n = Random.Range (0, 50);
			timer = 100 + n;
		}
	}
		
}
