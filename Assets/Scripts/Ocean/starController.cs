﻿using UnityEngine;
using System.Collections;

public class starController : MonoBehaviour {

	public  GameObject star;
	public float time;

	private int count = 0;
	private float temp = 0.0f;

	void Start () {
		Random.seed = System.Environment.TickCount;
		spawnStars ();
	}
		
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		temp += Time.deltaTime;
		if (temp > 3.0f) {
			temp = 0.0f;
			shrinkStars ();
		}
		if (!star.activeSelf){
			temp = 0.0f;
			spawnStars ();
		}
		if (count > 0) {
			count--;
			star.transform.localScale = new Vector3 (0.005f * count, 0.005f * count, 1);
			if (count == 0) {
				spawnStars ();
			}
		}
		if (count < 0) {
			count++;
			star.transform.localScale = new Vector3 (0.005f * (count+26), 0.005f * (count+26), 1);
		}
	}

	void shrinkStars(){
		count = 26;
	}

	void spawnStars(){
		star.SetActive (true);
		star.transform.localScale = new Vector3 (0f,0f,1f);
		float xPos = Random.Range (-8f, 8f);
		float yPos = Random.Range (-3f, 3f);
		star.transform.position = new Vector3 (xPos, yPos, 0f);
		count = -26;
	}
}
