﻿using UnityEngine;
using System.Collections;

public class BoundCheck : MonoBehaviour {

	private int count = 0;

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("wall"))
		{
			if (count == 0)
				count++;
			else {
				count = 0;
				gameObject.SetActive (false);
			}
		}
	}
}
