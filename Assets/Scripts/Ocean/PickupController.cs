﻿using UnityEngine;
using System.Collections;

public class PickupController : MonoBehaviour {

	private GameObject heart;
	private GameObject shield;
	private GameObject gold;
	private float heartTime;
	private float shieldTime;
	private float goldTime;

	private float curHeart = 0.0f;
	private float curShield = 0.0f;
	private float curGold = 0.0f;

	void Start () {
		heart = transform.GetChild (0).gameObject;
		shield = transform.GetChild (1).gameObject;
		gold = transform.GetChild (2).gameObject;
		heartTime = 15.0f;
		shieldTime = 18.0f;
		goldTime = 7.0f;
		Random.seed = System.Environment.TickCount;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		curHeart += Time.deltaTime;
		curShield += Time.deltaTime;
		curGold += Time.deltaTime;
		if (curHeart > heartTime) {
			curHeart = 0.0f;
			spawn (heart);
		}
		if (curHeart > 5.0f && heart.activeSelf) {
			heart.SetActive (false);
		}
		if (curShield > shieldTime) {
			curShield = 0.0f;
			spawn (shield);
		}
		if (curShield > 5.0f && shield.activeSelf) {
			shield.SetActive (false);
		}
		if (curGold > goldTime) {
			curGold = 0.0f;
			spawn (gold);
		}
		if (curGold > 5.0f && gold.activeSelf) {
			gold.SetActive (false);
		}
	}

	public void spawn (GameObject pickup) {
		pickup.SetActive (true);
		float xPos = Random.Range (-8f, 8f);
		float yPos = Random.Range (-3f, 3f);
		pickup.transform.position = new Vector3 (xPos, yPos, 0f);
	}
}
