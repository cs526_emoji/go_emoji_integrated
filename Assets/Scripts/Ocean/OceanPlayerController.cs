﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OceanPlayerController : MonoBehaviour {

	public AudioClip teleportSound;
	public AudioClip healSound;
	public AudioClip equipSound;
	public AudioClip starSound;
	public AudioClip moneySound;
	public AudioClip emojiDie;
	public AudioClip potionSound;
	public AudioClip bulletSound;
	public AudioClip freezeSound;

	private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.
	public Sprite pic1;
	public Sprite pic2;
	public Sprite pic3;
	public Sprite defenceMode;
	private SpriteRenderer last;
	private bool defenceModeFlag = false;
	private float defenseTime;
	private int starCount;
	public GameObject ui;

	private int hp;
	private int oldhp;
	private int maxhp;
	private bool healthChange;
	private Vector3 oldPos; //main healthBar position

	private int state; //normal = 0, die = 1, win = 2
	private int curtainTimer;
	private int resetCount;
	private bool flag_getMoney;

	private int potionCount;
	private int potionPow;
	private float freezeTime;
	private float curFreezeTime;
	private int freezeCount;
	private int freezeMaskTimer;
	private int cannonCount;
	private int cannonPow;
	private int cannonDelay;
	private int cannonIdx;
	public Camera cam;
	public GameObject yellowBalls;

	void Start()
	{
		if (!GlobalVariables.sceneStarted) {
			MusicController.GetInstance ().playMusic (4);
			GlobalVariables.sceneStarted = true;
		}
		state = 0;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		GlobalVariables.pause = false;
		ui.transform.Find ("mask").gameObject.SetActive (true);
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		rb2d = GetComponent<Rigidbody2D> ();
		maxhp = GlobalVariables.maxHP;
		hp = maxhp;
		oldhp = maxhp;
		healthChange = false;
		oldPos = ui.transform.Find ("hb_main").transform.position;
		curtainTimer = 25;
		resetCount = 0;
		flag_getMoney = false;

		defenseTime = 0.0f;
		starCount = 0;

		initActiveSkills ();

		#if UNITY_IPHONE || UNITY_ANDROID
		GlobalVariables.resumeButtonPushed = false;
		#endif
	}

	void Update(){
		if (curtainTimer > 0) {
			curtainTimer--;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (curtainTimer*4*0.01f);
			if (curtainTimer == 0) {
				ui.transform.Find ("mask").gameObject.SetActive (false);
			}
		}
		if(defenceModeFlag && defenseTime < 5.0f) {
			defenseTime += Time.deltaTime;
			if (defenseTime >= 5.0f) {
				defenceModeFlag = false;
				defenseTime = 0.0f;
				setEmojiPic ();
			}
		}
		if (state == 0 && starCount >= 10) {
			win ();
		}
		if (healthChange) {
			updateHBFrame ();
		}
		if (state > 0) {
			ui.transform.Find ("mask").gameObject.SetActive (true);
			resetCount++;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (resetCount*4*0.01f);
			if (resetCount == 25) {
				if (state == 1) {
					SceneManager.LoadScene ("stage4");
				} else {
					if (GlobalVariables.currentLevel == 3) {
						GlobalVariables.currentLevel = 4;
					}
					saveGame ();
					MusicController.GetInstance ().playMusic (0);
					SceneManager.LoadScene ("world_map");
				}
			}
		}
		if (flag_getMoney&&ui.transform.Find("moneyText").transform.localScale.x>0.5) {
			ui.transform.Find ("moneyText").transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f);
			if (ui.transform.Find ("moneyText").transform.localScale.x == 0.5f) {
				flag_getMoney = false;
			}
		}

		shootCannon ();
	}
		

	void FixedUpdate() {
		#if UNITY_IPHONE || UNITY_ANDROID
		float moveHorizontal = Input.acceleration.x;
		float moveVertical = Input.acceleration.y;
		#else
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		#endif

		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);
		Vector2 resistance = -rb2d.velocity;
		resistance.Normalize ();
		rb2d.AddForce (resistance * rb2d.velocity.magnitude * 5f);
		rb2d.AddForce (movement * 60f);

	}

	void LateUpdate (){
		if (GlobalVariables.freeze) {
			curFreezeTime += Time.deltaTime;
			if (curFreezeTime > freezeTime) {
				GlobalVariables.freeze = false;
				freezeMaskTimer = 50;
			}
		}
		if (freezeMaskTimer > 0) {
			freezeMaskTimer--;
			ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (freezeMaskTimer*0.006f);
			if (freezeMaskTimer == 0) {
				ui.transform.Find ("freezeMask").gameObject.SetActive (false);
			}
		}
		if (GlobalVariables.startFadeIn) {
			MusicController.GetInstance ().fadeIn (0.04f);
		}
		if (GlobalVariables.startFadeOut) {
			MusicController.GetInstance ().fadeOut (0.04f);
		}
	}

	//OnTriggerEnter2D is called whenever this object overlaps with a trigger collider.
	void OnTriggerEnter2D(Collider2D other) 
	{
		if (defenceModeFlag) {
			if (other.gameObject.CompareTag ("Heart")) {	
				other.gameObject.SetActive (false);
				hp = maxhp;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (healSound, 1.0f);
				}
			}
			else if (other.gameObject.CompareTag ("Star")) {
				other.gameObject.SetActive (false);
				getStar ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (starSound, 1.0f);
				}
			} 
			else if (other.gameObject.CompareTag ("Vortex")) {	
				other.GetComponent<vortexScript> ().transmit ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (teleportSound, 1.0f);
				}
			}
			else if (other.gameObject.CompareTag ("gold")) {
				other.gameObject.SetActive (false);
				int n = Random.Range (10,15);
				n = (int)(n * GlobalVariables.boost);
				getMoney (n);
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (moneySound, 1.0f);
				}
			}
		} else {
			//Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
			if (other.gameObject.CompareTag ("Heart")) {	
				other.gameObject.SetActive (false);
				hp = maxhp;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (healSound, 1.0f);
				}
			} else if (other.gameObject.CompareTag ("Vortex")) {	
				other.GetComponent<vortexScript> ().transmit ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (teleportSound, 1.0f);
				}
			} else if (other.gameObject.CompareTag ("Shark")&&!GlobalVariables.freeze) {	
				GlobalVariables.damageTaken += 20;
				other.gameObject.SetActive (false);
				if (hp > 20) {
					hp -= 20;
					updateHB ();
					if (GlobalVariables.sound) {
						GetComponent<AudioSource> ().Play ();
					}
				} else {
					hp = 0;
					updateHB ();
					die ();
				}
			} else if (other.gameObject.CompareTag ("Spider")&&!GlobalVariables.freeze) {	
				GlobalVariables.damageTaken += 10;
				other.gameObject.SetActive (false);
				if (hp > 10) {
					hp -= 10;
					updateHB ();
					if (GlobalVariables.sound) {
						GetComponent<AudioSource> ().Play ();
					}
				} else {
					hp = 0;
					updateHB ();
					die ();
				}
			} else if (other.gameObject.CompareTag ("Star")) {
				other.gameObject.SetActive (false);
				getStar ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (starSound, 1.0f);
				}
			} else if (other.gameObject.CompareTag ("Shield")) {	
				other.gameObject.SetActive (false);
				gameObject.GetComponent<SpriteRenderer> ().sprite = defenceMode;
				defenceModeFlag = true;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (equipSound, 1.0f);
				}
			}
			else if (other.gameObject.CompareTag ("gold")) {
				other.gameObject.SetActive (false);
				int n = Random.Range (10,15);
				n = (int)(n * GlobalVariables.boost);
				getMoney (n);
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (moneySound, 1.0f);
				}
			}
		}

	}


	void OnTriggerStay2D(Collider2D other){
		if (other.gameObject.CompareTag("myShark")&&!GlobalVariables.freeze)
		{
			GlobalVariables.damageTaken += 5;
			if (defenceModeFlag) {
				defenceModeFlag = false;
			}
			if (hp > 5) {
				hp -= 5;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
		}
	}

	void die(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (emojiDie, 1.0f);
		}
		GlobalVariables.deathCount++;
		state = 1;
	}

	void win(){
		MusicController.GetInstance ().musicOff ();
		GlobalVariables.winCount++;
		state = 2;
	}

	void updateHB(){
		setEmojiPic ();
		float dist = (1.0f - (float)hp / maxhp) * 60f * (Screen.width / 800.0f);
		float scale = (float)hp / maxhp * 0.2f;
		ui.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.2f, 1f);
		ui.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		if (oldhp > hp) {
			healthChange = true;
		} else if (oldhp < hp) {
			oldhp = hp;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	void updateHBFrame(){
		if (oldhp == hp) {
			healthChange = false;
		} else {
			oldhp--;
			float dist = (1.0f - (float)oldhp / maxhp) * 60f * (Screen.width / 800.0f);
			float scale = (float)oldhp / maxhp * 0.2f;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	void setEmojiPic(){
		SpriteRenderer spr = gameObject.GetComponent<SpriteRenderer> ();
		if (defenceModeFlag) {
			spr.sprite = defenceMode;
		} else if (hp > 0.6 * maxhp) {
			spr.sprite = pic1;
		} else if (hp > 0.2 * maxhp) {
			spr.sprite = pic2;
		} else {
			spr.sprite = pic3;
		}
	}

	public void getMoney(int val){
		GlobalVariables.moneyEarned += val;
		GlobalVariables.money += val;
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		ui.transform.Find ("moneyText").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		flag_getMoney = true;
	}

	void getStar(){
		starCount++;
		ui.transform.Find ("starText").GetComponent<Text> ().text = starCount.ToString() + " / 10";
	}

	void initActiveSkills(){
		GlobalVariables.freeze = false;
		potionCount = GlobalVariables.shopLevels [4];
		potionPow = GlobalVariables.potionPower;
		freezeTime = GlobalVariables.shopLevels [3] * 0.8f;
		curFreezeTime = 0.0f;
		freezeCount = GlobalVariables.shopLevels [3] > 0 ? 1 : 0;
		freezeMaskTimer = 0;
		cannonCount = GlobalVariables.shopLevels [5] > 0 ? 10 + 3 * GlobalVariables.shopLevels [5] : 0;
		cannonPow = GlobalVariables.shopLevels [5] * 5;
		cannonDelay = 0;
		cannonIdx = 0;
		if (potionCount > 0) {
			ui.transform.Find ("potion").gameObject.SetActive (true);
			ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		}
		if (freezeCount > 0) {
			ui.transform.Find ("hourglass").gameObject.SetActive (true);
			ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		}
		if (cannonCount > 0) {
			ui.transform.Find ("cannon").gameObject.SetActive (true);
			ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
		}
	}

	public void usePotion(){
		if (potionCount == 0) {
			return;
		}
		potionCount--;
		ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		hp += potionPow;
		if (hp > maxhp) {
			hp = maxhp;
		}
		updateHB ();
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (potionSound, 1.0f);
		}
	}

	public void useFreeze(){
		if (freezeCount == 0) {
			return;
		}
		freezeCount--;
		ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		ui.transform.Find ("freezeMask").gameObject.SetActive (true);
		ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (0.3f);
		GlobalVariables.freeze = true;
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (freezeSound, 1.0f);
		}
	}

	void shootCannon(){
		if (cannonDelay > 0) {
			cannonDelay--;
		}
		else if (!GlobalVariables.pause && cannonPow > 0 && cannonCount > 0) {
			#if UNITY_IPHONE || UNITY_ANDROID
			if (Input.touchCount > 0) {
				if(GlobalVariables.resumeButtonPushed){
					GlobalVariables.resumeButtonPushed = false;
					return;
				}
				Vector2 mp = Input.GetTouch (0).position;
				Vector3 wp = cam.ScreenToWorldPoint (new Vector3(mp.x,mp.y,0));
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.z = 0;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBallController>().reset();
				cur.transform.position = transform.position;
				cur.GetComponent<Rigidbody2D>().velocity = dir * 5.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#else
			if (Input.GetButtonDown ("Fire1")) {
				Vector3 mp = Input.mousePosition;
				Vector3 wp = cam.ScreenToWorldPoint (mp);
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.z = 0;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBallController>().reset();
				cur.transform.position = transform.position;
				cur.GetComponent<Rigidbody2D>().velocity = dir * 5.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#endif
		}
	}

	bool inButtonArea(Vector3 worldPoint){
		RectTransform CanvasRect = ui.GetComponent<RectTransform>();
		float canvasWidth = CanvasRect.sizeDelta.x;
		float canvasHeight = CanvasRect.sizeDelta.y;
		Vector3 viewPoint = cam.WorldToViewportPoint (worldPoint);
		Vector2 canvasPoint = new Vector2 (viewPoint.x * canvasWidth - canvasWidth * 0.5f, viewPoint.y * canvasHeight - canvasHeight * 0.5f);
		float dist;

		if (potionPow > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(100f,190f));
			if (dist < 25f)
				return true;
		}

		if (freezeTime > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(160f,190f));
			if (dist < 25f)
				return true;
		}

		dist = Vector2.Distance (canvasPoint,new Vector2(220f,190f));
		if (dist < 25f)
			return true;
		return false;

	}

	void saveGame(){
		SaveLoad sl = new SaveLoad ();
		sl.save ();
	}

}