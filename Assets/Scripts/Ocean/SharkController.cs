﻿using UnityEngine;
using System.Collections;

public class SharkController : MonoBehaviour {

	public GameObject shark;
	public GameObject player;
	public int delay;

	private int timer;

	void Start () {
		Random.seed = (int)(transform.position.x*100);
		int n = Random.Range (0, 50);
		timer = 100 + delay + n;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (timer > 90) {
			timer--;
		} else if (timer == 90) {
			shark.SetActive (true);
			shark.transform.position = transform.position;
			timer--;
		} else if (timer > 0) {
			shark.transform.position = transform.position;
			timer--;
		} else if (timer == 0) {
			float vX = player.transform.position.x - shark.transform.position.x;
			float vY = player.transform.position.y - shark.transform.position.y;
			Vector2 movement = new Vector2 (vX, vY);
			movement.Normalize ();
			shark.GetComponent<Rigidbody2D> ().velocity = movement;
			timer--;
		} else if (!shark.activeSelf){
			Random.seed = (int)(transform.position.x*100);
			int n = Random.Range (0, 50);
			timer = 100 + n;
		}
	}

}
