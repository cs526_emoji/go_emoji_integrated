﻿using UnityEngine;
using System.Collections;

public class vortexScript : MonoBehaviour {

	public GameObject player;
	public GameObject otherVortex;
	public float offset;
	private bool shut;
	private float temp;
	private int count;

	void Start () {
		shut = false;
		temp = 0.0f + offset;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		temp += Time.deltaTime;
		if (shut) {
			if (Vector3.Distance (player.transform.position, transform.position) > 1.2) {
				GetComponent<CircleCollider2D> ().enabled = true;
				shut = false;
			}
		}
		if (temp > 5.0f&&count==0) {
			count = 41;
		}
		if (count > 0) {
			count--;
			if (count > 20) {
				GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.05f * (count - 20));
			} else if (count == 20) {
				temp = 0.0f;
				Vector3 newPos;
				newPos.z = 1;
				do{
					newPos.x = Random.Range (-8f, 8f);
					newPos.y = Random.Range (-4f, 4f);
				}while(Vector3.Distance(newPos,otherVortex.transform.position)<5);
				transform.position = newPos;
			} else {
				GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.05f * (20 - count));
			}
		}
	}

	public void transmit(){
		player.transform.position = otherVortex.transform.position;
		otherVortex.GetComponent<vortexScript> ().shutGate ();
	}

	public void shutGate(){
		GetComponent<CircleCollider2D> ().enabled = false;
		shut = true;
	}
}
