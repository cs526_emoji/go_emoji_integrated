﻿using UnityEngine;
using System.Collections;

public class MySharkController : MonoBehaviour {

	public GameObject player;
	private Rigidbody2D rb2d;
	private Animator anim;
	private bool turning;
	private int facingDir;//1:right, -1:left
	private int timer;

	void Start () {
		anim = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
		turning = false;
		facingDir = 1;
		timer = 0;
	}

	void FixedUpdate(){
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		float moveH = (player.transform.position.x - transform.position.x);
		float moveV = (player.transform.position.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		Vector2 resistance = -rb2d.velocity;
		movement.Normalize ();
		resistance.Normalize ();
		rb2d.AddForce (resistance * rb2d.velocity.magnitude * 10f);
		rb2d.AddForce (movement * 20f);
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (timer > 0) {
			timer--;
			if (timer == 0) {
				Vector3 temp = transform.localScale;
				temp.x *= -1;
				transform.localScale = temp;
				turning = false;
				anim.SetBool ("turn", false);
			}
			return;
		}
		if (!turning) {
			float dist = Vector2.Distance (transform.position, player.transform.position);
			if (dist < 3) {
				anim.SetBool ("bite", true);
			} else {
				anim.SetBool ("bite", false);
			}
		}
		if ((player.transform.position.x < transform.position.x && facingDir == 1) ||
		    (player.transform.position.x > transform.position.x && facingDir == -1)) {
			facingDir = -facingDir;
			turning = true;
			anim.SetBool ("turn", true);
			timer = 10;
		} 
	
	}
}
