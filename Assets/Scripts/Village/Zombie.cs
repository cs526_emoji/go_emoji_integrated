﻿using UnityEngine;
using System.Collections;

public class Zombie : MonoBehaviour {

	private Animator animator;
	public Vector2 target;
	private Vector2 start;
	private Vector2 curTarget;
	private bool flag;
	private Rigidbody2D rb2d;
	private Vector2 flip;

	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;

	private int time;
	private bool isDie;

	public GameObject hb;
	public GameObject player;
	private int atkDelay;
	private int attackingCount;

	void Start () {
		Random.seed = System.Environment.TickCount;
		animator = GetComponent<Animator> ();
		rb2d = GetComponent<Rigidbody2D> ();
		start.x = transform.position.x;
		start.y = transform.position.y;
		flip = transform.localScale;
		curTarget = target;
		flag = true;
		timer = 0;
		maxhp = 100;
		hp = 100;
		oldhp = 100;
		healthChange = false;
		time = 0;
		isDie = false;
		atkDelay = 0;
		attackingCount = 0;
	}

	public void Attack() {
		if (atkDelay == 0 && attackingCount == 0) {
			atkDelay = Random.Range (1, 30);
		}
	}

	public void takeDmg(int val){
		timer = 0;
		if (hp > val) {
			hp -= val;
		} else {
			hp = 0;
			die();
		}
		updateHB ();
	}

	public void die() {
		animator.Play ("ZombieDie", -1, 0f);
		isDie = true;
		rb2d.velocity = new Vector2 (0f,0f);
		rb2d.isKinematic = true;
		transform.GetComponent<BoxCollider2D> ().enabled = false;
		transform.GetChild (0).gameObject.SetActive (false);
		int n = Random.Range (8, 12);
		n = (int)(n * GlobalVariables.boost);
		player.GetComponent<VillagePlayerController>().getMoney (n);
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
	}

	void Update () {
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (GlobalVariables.freeze) {
			return;
		}
		if (isDie == true) {
			time += 1;
			if (time == 45) {
				hb.SetActive (false);
				GlobalVariables.enemyKilled++;
				gameObject.SetActive (false);
			}
			return;
		}
		if (atkDelay > 0) {
			atkDelay--;
			if (atkDelay == 0) {
				animator.Play ("ZombieAttack", -1, 0f);
				transform.GetChild (0).gameObject.SetActive (true);
				attackingCount = 20;
			}
		}
		if (attackingCount > 0) {
			attackingCount--;
			if (attackingCount == 0) {
				animator.Play ("ZombieWalk", -1, 0f);
				transform.GetChild (0).gameObject.SetActive (false);
			}
		}

		animator.SetTrigger ("ZombieAtttoWalk");

		if (reach ()) {
			if (flag) {
				curTarget = start;
				flip.x *= -1;
				transform.localScale = flip;
			}
			else {
				curTarget = target;
				flip.x *= -1;
				transform.localScale = flip;
			}
			flag = !flag;
		}


	}

	void FixedUpdate () {
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		transform.localRotation = Quaternion.identity;
		float moveH = (curTarget.x - transform.position.x);
		float moveV = (curTarget.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		movement.Normalize ();
		if(rb2d.velocity.magnitude<0.5)
			rb2d.AddForce (movement);
	}

	bool reach() {
		return (Mathf.Abs(transform.position.x - curTarget.x)<0.1)&&(Mathf.Abs(transform.position.y - curTarget.y)<0.1);
	}

	public bool isDead(){
		return isDie;
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.2f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 22.5f * 0.01f;
		float scale = (float)hp / maxhp * 0.075f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 22.5f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.075f;
		hb.transform.position = transform.position + Vector3.up * 0.2f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.2f;
	}
		
}
