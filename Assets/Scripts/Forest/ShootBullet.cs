﻿using UnityEngine;
using System.Collections;

public class ShootBullet : MonoBehaviour {

	public GameObject bullet;
	public GameObject player;
	public int starttime;
	private int timer;
	private float temp; 


	void Start () {
		Random.seed = (int)(transform.position.x*transform.position.y*100)+System.Environment.TickCount;
		int n = Random.Range (0, 50);
		timer = 40 + n;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		temp += Time.deltaTime;
		if (temp > starttime) {
			if (timer > 30) {
				timer--;
			} else if (timer == 30) {						
				bullet.transform.position = transform.position;
				bullet.SetActive (true);
				Quaternion rotation = Quaternion.LookRotation (player.transform.position - transform.position, transform.TransformDirection (Vector3.back));
				bullet.transform.rotation = new Quaternion (0, 0, rotation.z, rotation.w);
				timer--;
			} else if (timer > 0) {
				Quaternion rotation = Quaternion.LookRotation (player.transform.position - transform.position, transform.TransformDirection (Vector3.back));
				bullet.transform.rotation = new Quaternion (0, 0, rotation.z, rotation.w);
				timer--;
			} else if (timer == 0) {
				float vX = player.transform.position.x - transform.position.x;
				float vY = player.transform.position.y - transform.position.y;
				Vector2 movement = new Vector2 (vX, vY);
				movement.Normalize ();
				bullet.GetComponent<Rigidbody2D> ().velocity = movement * 5;
				timer--;
			} else if (!bullet.activeSelf) {
				Random.seed = (int)(transform.position.x * transform.position.y * 100) + System.Environment.TickCount;
				int n = Random.Range (0, 50);
				timer = 40 + n;
			}
		}
	}
}
