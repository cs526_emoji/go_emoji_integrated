﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {
	public Text timerLabel;
	public GameObject heart;
	public GameObject shell;
	public GameObject LeftSprial;
	public GameObject RightSprial;
	public GameObject alert;
	public GameObject gold;

	private string timerText; 
	private float temp; 
	private bool flagShell = false;
	private bool flagHeart = false;
	private bool flagSprial = false;
	private bool flagGold = false;
	private float transparent = 0.5f;
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}

		temp += Time.deltaTime;
		timerText = "TIME LEFT:  " + (60 - (int)temp).ToString ();
		timerLabel.text = timerText; 

		if (!flagGold&&(int)temp % 8 == 7) {
			gold.SetActive (true);
			Random.seed = System.Environment.TickCount;
			float xPos = Random.Range (-8f, 8f);
			float yPos = Random.Range (-3f, 3f);
			gold.transform.position = new Vector3 (xPos, yPos, 0f);
			flagGold = true;
		}

		if (flagGold&&(int)temp % 8 == 4) {
			flagGold = false;
			gold.SetActive (false);
		}

		if (temp > 44 && temp < 46) {
			alert.SetActive (true);
			alert.transform.position = new Vector3 (0f, 0f, 0f);
			alert.GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, transparent);
			transparent -= 0.01f;
			alert.transform.localScale += new Vector3 (0.02f, 0.02f, 0);

		}

		if (!flagHeart&&(temp>25||temp>45)) {
			heart.SetActive (true);
			Random.seed = System.Environment.TickCount;
			float xPos = Random.Range (-8f, 8f);
			float yPos = Random.Range (-3f, 3f);
			heart.transform.position = new Vector3 (xPos, yPos, 0f);
			flagHeart = true;
		}

		if (flagHeart&&(temp>35||temp>55)) {
			flagHeart = false;
			heart.SetActive (false);
		}

		if (temp>10 && !flagShell) {
			shell.SetActive (true);
			Random.seed = System.Environment.TickCount;
			float xPos = Random.Range (-8f, 8f);
			float yPos = Random.Range (-3f, 3f);
			shell.transform.position = new Vector3 (xPos, yPos, 0f);
			flagShell = true;
		}
		if (temp>20 && flagShell) {
			shell.SetActive (false);
			flagShell = false;
		}

		if (temp>45 && !flagSprial) {
			LeftSprial.SetActive (true);
			RightSprial.SetActive (true);
			LeftSprial.transform.position = new Vector3 (-4.25f, 0f, 0f);
			RightSprial.transform.position = new Vector3 (4.25f, 0f, 0f);
			flagSprial = true;
		}

	}
		
}
