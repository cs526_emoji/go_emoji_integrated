﻿using UnityEngine;
using System.Collections;
using System;

public class Sprial : MonoBehaviour {

	public Rigidbody2D emoji;

	private float temp; 
	private float gravitationalForce;
	private Vector3 directionOfEmojiFromPlanet;

	// Use this for initialization
	void Start () {
		temp = 0;
		gravitationalForce = 0;
		directionOfEmojiFromPlanet = Vector3.zero;
		GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (transform.position.x < 8.0f) {
			temp += Time.deltaTime;
			int second = (int)temp;
			if (second > 0 && second < 5) {
				transform.localScale += new Vector3 (0.008f, 0.008f, 0);
				gravitationalForce += 0.8f;
			}
		}
	}

	void FixedUpdate ()
	{	
		if (GlobalVariables.freeze) {
			return;
		}
		if (transform.position.x < 8.0f) {
			float dist = (transform.position - emoji.transform.position).magnitude * 100f;
			if (dist < 30.0f)
				dist = 30.0f;
			directionOfEmojiFromPlanet = (transform.position - emoji.transform.position).normalized;
			emoji.AddForce (2.0f * directionOfEmojiFromPlanet * gravitationalForce / (float)Math.Sqrt(dist) );
		}
	}
		
}
