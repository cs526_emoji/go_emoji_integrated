﻿using UnityEngine;
using System.Collections;

public class ShootLights : MonoBehaviour {

	public GameObject light0;
	public GameObject light1;

	private float temp; 
	private int timestart = 20;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}

		temp += Time.deltaTime;
		int seconds = (int)temp;

		if (seconds == timestart) {
			light0.SetActive (true);
			light0.transform.position = transform.position;
		}
		if (seconds > timestart) {

			if (transform.position.y > 0) {
				light0.transform.position = new Vector3(light0.transform.position.x , light0.transform.position.y - Time.deltaTime * 1f, 0f);
			} else {
				light0.transform.position = new Vector3(light0.transform.position.x , light0.transform.position.y + Time.deltaTime * 1f, 0f);
			}

			if (light0.transform.position.y > 6.9f || light0.transform.position.y < -6.9f) {
				light0.transform.position = transform.position;
				light0.SetActive (true);
			}
				
				
		}

		if (seconds == timestart+5) {
			light1.SetActive (true);
			light1.transform.position = transform.position;
		}
		if (seconds > timestart+5) {

			if (transform.position.y > 0) {
				light1.transform.position = new Vector3(light1.transform.position.x, light1.transform.position.y - Time.deltaTime * 1f, 0f);
			} else {
				light1.transform.position = new Vector3(light1.transform.position.x, light1.transform.position.y + Time.deltaTime * 1f, 0f);
			}

			if (light1.transform.position.y > 6.9f || light1.transform.position.y < -6.9f) {
				light1.transform.position = transform.position;
				light1.SetActive (true);
			}

		}
			
	}
}
