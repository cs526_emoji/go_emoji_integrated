﻿using UnityEngine;
using System.Collections;

public class ShootLight2 : MonoBehaviour {

	public GameObject light0;
	public GameObject light1;
	public GameObject light2;
	public GameObject light3;

	private float temp; 
	private int starttime = 30;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}

		temp += Time.deltaTime;
		int seconds = (int)temp;

		if (seconds == starttime) {
			light0.SetActive (true);
			light0.transform.position = transform.position;
		}
		if (seconds > starttime) {

			shoot (light0);
		}

		if (seconds == starttime+5) {
			light1.SetActive (true);
			light1.transform.position = transform.position;
		}
		if (seconds > starttime+5) {

			shoot (light1);
		}

		if (seconds == starttime+10) {
			light2.SetActive (true);
			light2.transform.position = transform.position;
		}
		if (seconds > starttime+10) {

			shoot (light2);
		}


		if (seconds == starttime+15) {
			light3.SetActive (true);
			light3.transform.position = transform.position;
		}
		if (seconds > starttime+15) {

			shoot (light3);
		}
	
	}

	void shoot(GameObject light) {
		if (transform.position.x > 0) {
			light.transform.position = new Vector3(light.transform.position.x - Time.deltaTime * 1f, light.transform.position.y, 0f);
		} else {
			light.transform.position = new Vector3(light.transform.position.x + Time.deltaTime * 1f, light.transform.position.y, 0f);
		}

		if (light.transform.position.x > 11.7f || light0.transform.position.x < -11.7f) {
			light.transform.position = transform.position;
			light.SetActive (true);
		}
	}
}
