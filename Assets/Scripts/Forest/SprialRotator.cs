﻿using UnityEngine;
using System.Collections;

public class SprialRotator : MonoBehaviour {

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if(CompareTag("LeftSprial")) {
			transform.Rotate (new Vector3 (0, 0, -45) * Time.deltaTime * 2);
		}
		else if(CompareTag("RightSprial")) {
			transform.Rotate (new Vector3 (0, 0, 45) * Time.deltaTime * 2);
		}
		else {	
			transform.Rotate (new Vector3 (0, 0, -45) * Time.deltaTime);
		}
	}

}
