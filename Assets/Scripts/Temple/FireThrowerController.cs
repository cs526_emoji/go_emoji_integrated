﻿using UnityEngine;
using System.Collections;

public class FireThrowerController : MonoBehaviour {

	public GameObject stage_manager;
	public Stage2Player player;
	public GameObject fire;
	public GameObject hb;
	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;

	private int fireInterval;
	public int fireDelay;
	private float fireTimer;

	void Start () {
		fireInterval = 100 + fireDelay;
		fireTimer = 0.0f;
		timer = 0;
		maxhp = 150;
		hp = 150;
		oldhp = 150;
		healthChange = false;
	}

	void Update() {
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (GlobalVariables.freeze) {
			return;
		}
		if (fireInterval > 0 && !GlobalVariables.pause) {
			fireInterval--;
			if (fireInterval == 0) {
				fire.SetActive (true);
				fireTimer += Time.deltaTime;
			}
		} else {
			fireTimer += Time.deltaTime;
			if (fireTimer > 1.35) {
				fire.GetComponent<FireColliderController> ().reset ();
				fire.SetActive (false);
				fireTimer = 0;
				fireInterval = 100;
			}
		}
	}

	public void takeDmg(int val){
		if (hp > val) {
			timer = 0;
			hp -= val;
			updateHB ();
		} else {
			die();
		}
	}

	void die(){
		hb.SetActive (false);
		gameObject.SetActive(false);
		if (fire.activeSelf) {
			fire.GetComponent<SelfDestruction> ().enabled = true;
			fire.GetComponent<SelfDestruction> ().selfDestruct (1.35f - fireTimer);
		}
		int n = Random.Range (25, 35);
		n = (int)(n * GlobalVariables.boost);
		player.getMoney (n);
		GlobalVariables.enemyKilled++;
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 22.5f * 0.01f;
		float scale = (float)hp / maxhp * 0.075f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 22.5f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.075f;
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.3f;
	}
}
