﻿using UnityEngine;
using System.Collections;

public class SelfDestruction : MonoBehaviour {

	private float timeLeft = 100;

	public void selfDestruct(float time){
		timeLeft = time;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		timeLeft -= Time.deltaTime;
		if (timeLeft < 0) {
			gameObject.SetActive(false);
		}
	}
}
