﻿using UnityEngine;
using System.Collections;

public class FireColliderController : MonoBehaviour {

	private float timer;
	private Vector2 oldSize;
	private Vector2 oldPos;
	BoxCollider2D bc;

	void Start () {
		timer = 0.0f;
		bc = GetComponent<BoxCollider2D> ();
		oldSize = new Vector2 (0.2f,0.4f);
		oldPos = new Vector2 (0.01f, -0.17f);
	}
		
	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		timer += Time.deltaTime;
		if (timer < 0.6f) {
			bc.size += new Vector2 (0f, 0.016f);
			bc.offset += new Vector2 (0.0f, 0.008f);
		} else if (timer > 1.0f) {
			bc.size -= new Vector2 (0.015f,0f);
		}
	}

	public void reset (){
		timer = 0.0f;
		bc.size = oldSize;
		bc.offset = oldPos;
	}
}
