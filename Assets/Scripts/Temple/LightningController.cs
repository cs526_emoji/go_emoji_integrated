﻿using UnityEngine;
using System.Collections;

public class LightningController : MonoBehaviour {

	private int count;
	public GameObject player;

	void Start () {
		count = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.pause || GlobalVariables.freeze) {
			return;
		}
		if(count<=60){
			if(count%30==0){
				transform.GetChild (count / 30).gameObject.SetActive (true);
			}
			count++;
		}
		if (player.transform.position.x > transform.position.x) {
			transform.position += new Vector3 (0.009f, 0f, 0f);
		} else {
			transform.position -= new Vector3 (0.009f, 0f, 0f);
		}
	}
}
