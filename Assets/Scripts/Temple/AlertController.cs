﻿using UnityEngine;
using System.Collections;

public class AlertController : MonoBehaviour {

	private int delay;
	private int count;

	void Start () {
		delay = 60;
		count = 60;
	}
	

	void Update () {
		if (GlobalVariables.pause||GlobalVariables.freeze) {
			return;
		}
		if (delay > 0) {
			delay--;
			return;
		}
		if (count > 0) {
			count--;
			if (count == 0) {
				gameObject.SetActive (false);
			} else if (count / 10 % 2 == 1) {
				transform.GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 1.0f - count%10*0.1f);
			} else {
				transform.GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, count%10*0.1f);
			}
				
		}
	}
}
