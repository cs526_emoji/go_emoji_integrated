﻿using UnityEngine;
using System.Collections;

public class MovingStoneController : MonoBehaviour {

	private int pause;
	private int count;
	public int delay;
	public float step;

	// Use this for initialization
	void Start () {
		pause = delay;
		count = 20;
	}
	
	// Update is called once per frame
	void Update () {
		if (GlobalVariables.pause || GlobalVariables.freeze)
			return;
		if (pause > 0) {
			pause--;
			return;
		}
		if (count == 0) {
			count = 20;
			step = -step;
			pause = 50;
		} else {
			transform.position += new Vector3 (0, step, 0);
			count--;
		}
	}
}
