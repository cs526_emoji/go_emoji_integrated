﻿using UnityEngine;
using System.Collections;

public class StoneDoorController : MonoBehaviour {

	private int state;
	private int timer;
	private float step;

	private GameObject up;
	private GameObject down;
	private GameObject locker;

	void Start () {
		state = 0;
		timer = 32;
		step = 0.01f;
		up = transform.Find ("door_up").gameObject;
		down = transform.Find ("door_down").gameObject;
		locker = transform.Find ("lock").gameObject;
	}

	void Update () {
		if (state == 0 || GlobalVariables.freeze)
			return;
		if (timer > 0) {
			float alpha = timer * step * 3;
			up.transform.position += new Vector3 (0f, step, 0f);
			down.transform.position -= new Vector3 (0f, step, 0f);
			locker.transform.localScale += new Vector3 (step, step, 0f);
			locker.transform.GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, alpha);
			timer--;
		} else {
			locker.SetActive (false);
			state = 0;
		}
	}

	public void openDoor(){
		state = 1;
	}
}
