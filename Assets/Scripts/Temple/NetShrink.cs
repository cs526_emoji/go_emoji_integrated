﻿using UnityEngine;
using System.Collections;

public class NetShrink : MonoBehaviour {

	private int count;
	private int pause;
	private float step;
	public int delay;


	void Start() {
		step = 0.003f;
		count = 20;
		pause = delay;
	}

	void Update () {
		if (GlobalVariables.pause || GlobalVariables.freeze)
			return;
		if (pause > 0) {
			pause--;
			return;
		}
		if (count == 0) {
			count = 20;
			step = -step;
			pause = 70;
		} else {
			transform.localScale += new Vector3 (step, step, 0);
			count--;
		}
	}
}
