﻿using UnityEngine;
using System.Collections;

public class ShootFreeze : MonoBehaviour {
	
	private Vector2 velo;
	private float angV;
	private Rigidbody2D rb2d;
	private bool freeze;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		freeze = false;
	}

	void Update () {
		if (!freeze && GlobalVariables.freeze) {
			freeze = true;
			velo = rb2d.velocity;
			angV = rb2d.angularVelocity;
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
		}
		if (freeze && !GlobalVariables.freeze) {
			freeze = false;
			rb2d.velocity = velo;
			rb2d.angularVelocity = angV;
		}
	}
}
