﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopActions : MonoBehaviour {

	public AudioClip turnpage;
	public AudioClip purchase;

	private int timer;
	public GameObject page_passive;
	public GameObject page_active;
	private int moveFlag; //1: move left, -1: move right 
	private int moveCount;
	private int pageState; //0:passive, 1:active

	void Start () {
		updateMoneyText ();
		setUpPages ();
		timer = 0;
		moveFlag = 0;
		moveCount = 0;
		pageState = 0;
		transform.Find ("mask").gameObject.SetActive (true);
	}

	void Update () {
		if (timer < 26) {
			float alpha = (float)(25 - timer) / 25.0f;
			transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (alpha);
			timer++;
			if (timer == 26) {
				transform.Find ("mask").gameObject.SetActive (false);
			}
		}
		if (moveFlag != 0) {
			page_active.transform.position -= new Vector3 (moveFlag * 80f * (Screen.width / 1024.0f),0,0);
			page_passive.transform.position -= new Vector3 (moveFlag * 80f * (Screen.width / 1024.0f),0,0);
			moveCount--;
			if (moveCount == 0) {
				moveFlag = 0;
				if (pageState == 0) {
					page_active.SetActive (false);
				} else {
					page_passive.SetActive (false);
				}
			}
		}
	}

	void updateMoneyText(){
		transform.Find("moneyText").GetComponent<Text>().text = GlobalVariables.money.ToString ();
	}

	public void nextPage(int direction){
		if (moveFlag != 0) {
			return;
		}
		if (pageState == 0) {
			page_active.transform.position = page_passive.transform.position + new Vector3 (direction * 1120f * (Screen.width / 1024.0f), 0, 0);
			page_active.SetActive (true);
			pageState = 1;
		} else {
			page_passive.transform.position = page_active.transform.position + new Vector3 (direction * 1120f * (Screen.width / 1024.0f), 0, 0);
			page_passive.SetActive (true);
			pageState = 0;
		}
		moveFlag = direction;
		moveCount = 14;
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (turnpage, 1.0f);
		}
	}

	void setUpPages(){
		for (int i = 0; i < 6; i++) {
			updateDescription (i, GlobalVariables.shopLevels [i]);
		}
	}

	public void levelUp(int id){
		int curLv = GlobalVariables.shopLevels[id];
		doLevelUP (id,curLv+1);
	}

	void doLevelUP(int id, int lv){
		int curMoney = GlobalVariables.money;
		switch (id) {
		case 0:
			{
				if (curMoney < 100 * lv) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= 100 * lv;
				GlobalVariables.moneySpent += 100 * lv;
				GlobalVariables.damage = 10 + lv * 5;
				GlobalVariables.shopLevels [0] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		case 1:
			{
				int price = 90 * lv + 10 * lv * lv;
				if (curMoney < price) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= price;
				GlobalVariables.moneySpent += price;
				GlobalVariables.maxHP = 100 + lv * 20;
				GlobalVariables.shopLevels [1] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		case 2:
			{
				int price = 90 * lv + 10 * lv * lv;
				if (curMoney < price) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= price;
				GlobalVariables.moneySpent += price;
				GlobalVariables.boost = 1.0f + lv * 0.3f;
				GlobalVariables.shopLevels [2] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		case 3:
			{
				int price = 300 * lv;
				if (curMoney < price) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= price;
				GlobalVariables.moneySpent += price;
				GlobalVariables.shopLevels [3] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		case 4:
			{
				int price = 150 * lv * lv - 50 * lv;
				if (curMoney < price) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= price;
				GlobalVariables.moneySpent += price;
				GlobalVariables.potionPower = lv * 30 + 20;
				GlobalVariables.shopLevels [4] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		case 5:
			{
				int price = 300 * lv;
				if (curMoney < price) {
					break;
				}
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (purchase, 1.0f);
				}
				GlobalVariables.money -= price;
				GlobalVariables.moneySpent += price;
				GlobalVariables.shopLevels [5] = lv;
				updateMoneyText ();
				updateDescription (id, lv);
				break;
			}
		}
	}

	void updateDescription(int id, int lv){
		switch (id) {
		case 0:
			{
				page_passive.transform.Find ("atk").Find ("atk_text").GetComponent<Text> ().text = "Current Damage: " + GlobalVariables.damage.ToString ();
				page_passive.transform.Find ("atk").Find ("atk_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 10) {
					page_passive.transform.Find ("atk").Find ("atk_text (1)").GetComponent<Text> ().text = "So Powerful";
					page_passive.transform.Find ("atk").Find ("atk_bt").gameObject.SetActive (false);
					page_passive.transform.Find ("max_text").gameObject.SetActive (true);
				} else {
					int nextDamage = GlobalVariables.damage + 5;
					int price = (lv + 1) * 100;
					page_passive.transform.Find ("atk").Find ("atk_text (1)").GetComponent<Text> ().text = "Next Level: " + nextDamage.ToString ();
					page_passive.transform.Find ("atk").Find ("atk_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		case 1:
			{
				page_passive.transform.Find ("hp").Find ("hp_text").GetComponent<Text> ().text = "Current HP: " + GlobalVariables.maxHP.ToString ();
				page_passive.transform.Find ("hp").Find ("hp_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 10) {
					page_passive.transform.Find ("hp").Find ("hp_text (1)").GetComponent<Text> ().text = "Sturdy as a Rock";
					page_passive.transform.Find ("hp").Find ("hp_bt").gameObject.SetActive (false);
					page_passive.transform.Find ("max_text (1)").gameObject.SetActive (true);
				} else {
					int nextHP = GlobalVariables.maxHP + 20;
					int price = 10 * lv * lv + 110 * lv + 100;
					page_passive.transform.Find ("hp").Find ("hp_text (1)").GetComponent<Text> ().text = "Next Level: " + nextHP.ToString ();
					page_passive.transform.Find ("hp").Find ("hp_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		case 2:
			{
				page_passive.transform.Find ("money").Find ("money_text").GetComponent<Text> ().text = "Current Boost: " + GlobalVariables.boost.ToString ("#.0") + "x";
				page_passive.transform.Find ("money").Find ("money_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 10) {
					page_passive.transform.Find ("money").Find ("money_text (1)").GetComponent<Text> ().text = "Wealthy~";
					page_passive.transform.Find ("money").Find ("money_bt").gameObject.SetActive (false);
					page_passive.transform.Find ("max_text (2)").gameObject.SetActive (true);
				} else {
					float nextBoost = GlobalVariables.boost + 0.3f;
					int price = 10 * lv * lv + 110 * lv + 100;
					page_passive.transform.Find ("money").Find ("money_text (1)").GetComponent<Text> ().text = "Next Level: " + nextBoost.ToString ("#.0") + "x";
					page_passive.transform.Find ("money").Find ("money_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		case 3:
			{
				page_active.transform.Find ("time").Find ("time_text (1)").GetComponent<Text> ().text = "Becomes invincible for " + (0.8 * lv).ToString () + "s";
				page_active.transform.Find ("time").Find ("time_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 5) {
					page_active.transform.Find ("time").Find ("time_text (3)").GetComponent<Text> ().text = "Freeze Them All!";
					page_active.transform.Find ("time").Find ("time_bt").gameObject.SetActive (false);
					page_active.transform.Find ("max_text (3)").gameObject.SetActive (true);
				} else {
					int price = 300 * (lv + 1);
					page_active.transform.Find ("time").Find ("time_text (3)").GetComponent<Text> ().text = "Next Level: " + (0.8 * lv + 0.8).ToString() + "s";
					page_active.transform.Find ("time").Find ("time_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		case 4:
			{
				page_active.transform.Find ("potion").Find ("potion_text").GetComponent<Text> ().text = "Recover " + GlobalVariables.potionPower.ToString() + " HP";
				page_active.transform.Find ("potion").Find ("potion_text (1)").GetComponent<Text> ().text = "Potion count: " + lv.ToString ();
				page_active.transform.Find ("potion").Find ("potion_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 5) {
					page_active.transform.Find ("potion").Find ("potion_text (3)").GetComponent<Text> ().text = "Safe and Sound";
					page_active.transform.Find ("potion").Find ("potion_bt").gameObject.SetActive (false);
					page_active.transform.Find ("max_text (4)").gameObject.SetActive (true);
				} else {
					int nextPower = 30 * lv + 50;
					int price = 140 * lv * lv + 250 * lv + 100;
					page_active.transform.Find ("potion").Find ("potion_text (3)").GetComponent<Text> ().text = "Next Level: " + nextPower.ToString() + " HP x" + (lv + 1).ToString();
					page_active.transform.Find ("potion").Find ("potion_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		case 5:
			{	
				page_active.transform.Find ("cannon").Find ("cannon_text").GetComponent<Text> ().text = "Shoot bullets of " + (5 * lv).ToString () + " damage";
				page_active.transform.Find ("cannon").Find ("cannon_text (2)").GetComponent<Text> ().text = "LV." + lv.ToString ();
				if (lv == 5) {
					page_active.transform.Find ("cannon").Find ("cannon_text (3)").GetComponent<Text> ().text = "PIU~PIU~";
					page_active.transform.Find ("cannon").Find ("cannon_bt").gameObject.SetActive (false);
					page_active.transform.Find ("max_text (5)").gameObject.SetActive (true);
				} else {
					int nextDamage = 5 * (lv + 1);
					int price = 300 * (lv + 1);
					page_active.transform.Find ("cannon").Find ("cannon_text (3)").GetComponent<Text> ().text = "Next Level Damage: " + nextDamage.ToString ();
					page_active.transform.Find ("cannon").Find ("cannon_bt").Find ("text").GetComponent<Text> ().text = price.ToString ();
				}

				break;
			}
		}
	}
}
