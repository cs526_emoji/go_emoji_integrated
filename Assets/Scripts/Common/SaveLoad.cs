﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveLoad {

	private SavedData savedData;

	public SaveLoad(){
		savedData = new SavedData ();
	}

	public void save() {
		savedData.pack ();
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedData.gd");
		bf.Serialize(file, savedData);
		file.Close();
	}

	public void load() {
		if(File.Exists(Application.persistentDataPath + "/savedData.gd")) {
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/savedData.gd", FileMode.Open);
			savedData = (SavedData) bf.Deserialize(file);
			savedData.unpack ();
			file.Close();
		}
	}
}

