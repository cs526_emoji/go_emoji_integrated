﻿using UnityEngine;
using System.Collections;

public class FreezeShoot3D : MonoBehaviour {

	private Vector3 velo;
	private Vector3 angV;
	private Rigidbody rb;
	private bool freeze;

	void Start () {
		rb = GetComponent<Rigidbody> ();
		freeze = false;
	}

	void Update () {
		if (!freeze && GlobalVariables.freeze) {
			freeze = true;
			velo = rb.velocity;
			angV = rb.angularVelocity;
			rb.velocity = Vector3.zero;
			rb.angularVelocity = Vector3.zero;
		}
		if (freeze && !GlobalVariables.freeze) {
			freeze = false;
			rb.velocity = velo;
			rb.angularVelocity = angV;
		}
	}

}
