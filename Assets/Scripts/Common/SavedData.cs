﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable] 

public class SavedData
{
	private int currentLevel;
	private int money;
	private int maxHP; 
	private int damage;
	private float boost;
	private List<int> shopLevels;
	private int potionPower;

	private bool music = true;
	private bool sound = true;

	private float gameTime;
	private int enemyKilled;
	private int winCount;
	private int deathCount;
	private int moneyEarned;
	private int moneySpent;
	private int damageDealt;
	private int damageTaken;

	public void pack(){
		currentLevel = GlobalVariables.currentLevel;
		money = GlobalVariables.money;
		maxHP = GlobalVariables.maxHP;
		damage = GlobalVariables.damage;
		boost = GlobalVariables.boost;
		shopLevels = GlobalVariables.shopLevels;
		potionPower = GlobalVariables.potionPower;
		music = GlobalVariables.music;
		sound = GlobalVariables.sound;
		gameTime = GlobalVariables.gameTime + Time.realtimeSinceStartup;
		enemyKilled = GlobalVariables.enemyKilled;
		winCount = GlobalVariables.winCount;
		deathCount = GlobalVariables.deathCount;
		moneyEarned = GlobalVariables.moneyEarned;
		moneySpent = GlobalVariables.moneySpent;
		damageDealt = GlobalVariables.damageDealt;
		damageTaken = GlobalVariables.damageTaken;
	}

	public void unpack(){
		GlobalVariables.currentLevel = currentLevel;
		GlobalVariables.money = money;
		GlobalVariables.maxHP = maxHP;
		GlobalVariables.damage = damage;
		GlobalVariables.boost = boost;
		GlobalVariables.shopLevels = shopLevels;
		GlobalVariables.potionPower = potionPower;
		GlobalVariables.music = music;
		GlobalVariables.sound = sound;
		GlobalVariables.gameTime = gameTime;
		GlobalVariables.enemyKilled = enemyKilled;
		GlobalVariables.winCount = winCount;
		GlobalVariables.deathCount = deathCount;
		GlobalVariables.moneyEarned = moneyEarned;
		GlobalVariables.moneySpent = moneySpent;
		GlobalVariables.damageDealt = damageDealt;
		GlobalVariables.damageTaken = damageTaken;
	}
}

