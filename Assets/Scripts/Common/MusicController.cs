﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicController : MonoBehaviour {
	private static MusicController instance;
	private AudioSource aus;

	public List<AudioClip> audioClips;

	public static MusicController GetInstance(){
		return instance;
	}

	void Awake() {
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			instance = this;
			aus = transform.GetComponent<AudioSource> ();
		}
		DontDestroyOnLoad(transform.gameObject);
	}

	public void playMusic(int idx){
		aus.Stop ();
		aus.clip = audioClips [idx];
		if (idx == 0) {
			aus.time = GlobalVariables.musicPos;
		} else {
			aus.time = 0.0f;
		}
		if (GlobalVariables.music) {
			if (idx > 0)
				aus.volume = 1.0f;
			else {
				aus.volume = 0.0f;
				musicOn ();
			}
		}
		aus.Play ();
	}

	public void musicOff(){
		GlobalVariables.startFadeOut = true;
	}

	public void musicOn(){
		GlobalVariables.startFadeIn = true;
	}

	public void pause (){
		aus.Pause ();
	}

	public void resume(){
		aus.Play ();
	}

	public void getPos(){
		GlobalVariables.musicPos = aus.time;
	}

	public void fadeOut(float val){
		if (aus.volume > val)
			aus.volume -= val;
		else {
			aus.volume = 0.0f;
			GlobalVariables.startFadeOut = false;
		}
	}

	public void fadeIn(float val){
		if (aus.volume + val < 1.0f)
			aus.volume += val;
		else {
			aus.volume = 1.0f;
			GlobalVariables.startFadeIn = false;
		}
	}
}
