﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldMapActions : MonoBehaviour {

	private int timer;

	void Start () {
		setUpStage ();
		timer = 0;
		transform.Find ("mask").gameObject.SetActive (true);
	}

	void Update () {
		if (timer < 26) {
			float alpha = (float)(25 - timer) / 25.0f;
			transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (alpha);
			timer++;
			if (timer == 26) {
				transform.Find ("mask").gameObject.SetActive (false);
			}
		}
		if (GlobalVariables.startFadeIn) {
			MusicController.GetInstance ().fadeIn (0.04f);
		}
		if (GlobalVariables.startFadeOut) {
			MusicController.GetInstance ().fadeOut (0.04f);
		}
	}

	void setUpStage(){
		int cur = GlobalVariables.currentLevel;
		for (int i = 0; i < 7; i++) {
			if (i < cur) {
				transform.Find ("levelButtons").transform.GetChild (i).GetComponent<Selectable> ().interactable = true;
				transform.Find ("levelButtons").transform.GetChild (i).GetComponent<Shrink> ().enabled = false;
				transform.Find ("locks").transform.GetChild (i).gameObject.SetActive (false);
			} else if (i == cur) {
				transform.Find ("levelButtons").transform.GetChild (i).GetComponent<Selectable> ().interactable = true;
				transform.Find ("levelButtons").transform.GetChild (i).GetComponent<Shrink> ().enabled = true;
				transform.Find ("locks").transform.GetChild (i).gameObject.SetActive (false);
			}
		}
		transform.Find("moneyText").GetComponent<Text>().text = GlobalVariables.money.ToString ();
	}
}
