﻿using UnityEngine;
using System.Collections;

public class FreezeAnimation : MonoBehaviour {

	private Animator anim;
	private bool freeze;

	void Start () {
		anim = GetComponent<Animator> ();
	}
	

	void Update () {
		if (!freeze && GlobalVariables.freeze) {
			freeze = true;
			anim.enabled = false;
		}
		if (freeze && !GlobalVariables.freeze) {
			freeze = false;
			anim.enabled = true;
		}
	}
}
