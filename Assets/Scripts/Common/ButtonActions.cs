﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class ButtonActions : MonoBehaviour {

	public GameObject ui;
	private GameObject mask;
	private int timer;
	private string scene;
	private bool q;
	private int stats; //0:nothing 1:showStats 2:back 3:reset
	private bool flag;
	private bool showingStats;

	void Start(){
		mask = ui.transform.FindChild ("mask").gameObject;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		timer = 0;
		q = false;
		stats = 0;
		flag = false;
		showingStats = false;
	}

	void Update(){
		if (timer > 0) {
			timer--;
			if (flag) {
				mask.GetComponent<CanvasRenderer> ().SetAlpha (timer * 0.04f);
			} else {
				mask.GetComponent<CanvasRenderer> ().SetAlpha ((100f - 4 * timer) * 0.01f);
			}
			if (timer == 0) {
				if (flag) {
					flag = false;
					mask.SetActive (false);
				} else if (q) {
					SaveLoad sl = new SaveLoad ();
					sl.save ();
					Application.Quit ();
				} else if (stats == 1) {
					stats = 0;
					ui.transform.FindChild ("page_stats").gameObject.SetActive (true);
					doShowStats ();
					showingStats = true;
					flag = true;
					timer = 25;
				} else if (stats == 2) {
					stats = 0;
					ui.transform.FindChild ("page_stats").gameObject.SetActive (false);
					ui.transform.FindChild ("page_reset").gameObject.SetActive (false);
					showingStats = false;
					flag = true;
					timer = 25;
				} else if (stats == 3) {
					stats = 0;
					ui.transform.FindChild ("page_reset").gameObject.SetActive (true);
					flag = true;
					timer = 25;
				} else {
					SceneManager.LoadScene (scene);
				}
			}
		}
		if (showingStats) {
			updateTime ();
		}
	}

	public void loadScene(string name){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		scene = name;
		timer = 25;
		mask.SetActive (true);
	}

	public void loadLevel(string name){
		MusicController.GetInstance ().musicOff ();
		MusicController.GetInstance ().getPos ();
		GlobalVariables.sceneStarted = false;
		loadScene (name);
	}

	public void exit(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		q = true;
		timer = 25;
		mask.SetActive (true);
	}

	public void showStats(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		stats = 1;
		timer = 25;
		mask.SetActive (true);
	}

	public void reset(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		stats = 3;
		timer = 25;
		mask.SetActive (true);
	}

	public void back(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		stats = 2;
		timer = 25;
		mask.SetActive (true);
	}

	void doShowStats(){
		GameObject pageStats = ui.transform.FindChild ("page_stats").gameObject;
		pageStats.transform.GetChild (2).GetComponent<Text> ().text = "Total Time Played: " + floatToTime (GlobalVariables.gameTime + Time.realtimeSinceStartup);
		pageStats.transform.GetChild (3).GetComponent<Text> ().text = "Enemy Killed: " + GlobalVariables.enemyKilled.ToString();
		pageStats.transform.GetChild (4).GetComponent<Text> ().text = "Win Count: " + GlobalVariables.winCount.ToString();
		pageStats.transform.GetChild (5).GetComponent<Text> ().text = "Death Count: " + GlobalVariables.deathCount.ToString();
		pageStats.transform.GetChild (6).GetComponent<Text> ().text = "Money Earned: " + GlobalVariables.moneyEarned.ToString();
		pageStats.transform.GetChild (7).GetComponent<Text> ().text = "Money Spent: " + GlobalVariables.moneySpent.ToString();
		pageStats.transform.GetChild (8).GetComponent<Text> ().text = "Damage Dealt: " + GlobalVariables.damageDealt.ToString();
		pageStats.transform.GetChild (9).GetComponent<Text> ().text = "Damage Taken: " + GlobalVariables.damageTaken.ToString();
	}

	string floatToTime(float input){
		string hour = Mathf.Floor(input / 3600).ToString();
		string minute = Mathf.Floor(input % 3600 / 60).ToString();
		string second = Mathf.Floor(input % 60).ToString();
		return hour + "h " + minute + "m " + second + "s";
	}

	public void doReset(){
		GlobalVariables.currentLevel = 0;
		GlobalVariables.money = 0;
		GlobalVariables. maxHP = 100; 
		GlobalVariables.damage = 10;
		GlobalVariables.boost = 1.0f;
		GlobalVariables.shopLevels = new List<int> {0,0,0,0,0,0};
		GlobalVariables.potionPower = 0;

		GlobalVariables.music = true;
		GlobalVariables.sound = true;

		GlobalVariables.gameTime = -Time.realtimeSinceStartup;
		GlobalVariables.enemyKilled = 0;
		GlobalVariables.winCount = 0;
		GlobalVariables.deathCount = 0;
		GlobalVariables.moneyEarned = 0;
		GlobalVariables.moneySpent = 0;
		GlobalVariables.damageDealt = 0;
		GlobalVariables.damageTaken = 0;

		SaveLoad sl = new SaveLoad ();
		sl.save ();

		stats = 2;
		timer = 25;
		mask.SetActive (true);
	}

	void updateTime(){
		GameObject pageStats = ui.transform.FindChild ("page_stats").gameObject;
		pageStats.transform.GetChild (2).GetComponent<Text> ().text = "Total Time Played: " + floatToTime (GlobalVariables.gameTime + Time.realtimeSinceStartup);
	}
}
