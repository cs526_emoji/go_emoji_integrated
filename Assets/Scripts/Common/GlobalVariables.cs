﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GlobalVariables
{
	public static bool applicationStarted = false;
	public static bool sceneStarted = false;
	public static bool pause = false;
	public static bool freeze = false;
	public static float musicPos = 0;
	public static bool startFadeIn = false;
	public static bool startFadeOut = false;

	public static int currentLevel = 0;
	public static int money = 0;
	public static int maxHP = 100; 
	public static int damage = 10;
	public static float boost = 1.0f;
	public static List<int> shopLevels = new List<int> {0,0,0,0,0,0};
	public static int potionPower = 0;

	public static bool music = true;
	public static bool sound = true;

	public static float gameTime = 0.0f;
	public static int enemyKilled = 0;
	public static int winCount = 0;
	public static int deathCount = 0;
	public static int moneyEarned = 0;
	public static int moneySpent = 0;
	public static int damageDealt = 0;
	public static int damageTaken = 0;

	#if UNITY_IPHONE || UNITY_ANDROID
	public static bool resumeButtonPushed = false;
	#endif
}






