﻿using UnityEngine;
using System.Collections;

public class Shrink : MonoBehaviour {

	private int count;
	private float step;


	void Start() {
		step = 0.002f;
		count = 20;
	}

	void Update () {
		if (count == 0) {
			count = 20;
			step = -step;
		}
		transform.localScale += new Vector3 (step, step, 0);
		count--;
	}
}
