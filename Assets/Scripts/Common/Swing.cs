﻿using UnityEngine;
using System.Collections;

public class Swing : MonoBehaviour {

	private int count;
	private float step;


	void Start() {
		step = 0.5f;
		if (transform.position.x < 500) {
			step = -step;
		}
		count = 30;
	}

	void Update () {
		if (count == 0) {
			count = 30;
			step = -step;
		}
		transform.position += new Vector3 (step, 0, 0);
		count--;
	}
}
