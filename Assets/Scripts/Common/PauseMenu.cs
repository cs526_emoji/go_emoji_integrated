﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {
	public GameObject ui;
	
	void Update () {
		if (Input.GetKeyDown ("escape")) {
			if (!GlobalVariables.pause) {
				MusicController.GetInstance ().pause ();
				GlobalVariables.pause = true;
				Time.timeScale = 0;
				ui.transform.Find ("mask").gameObject.SetActive (true);
				ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (0.5f);
				ui.transform.Find ("bt_resume").gameObject.SetActive (true);
				ui.transform.Find ("bt_menu").gameObject.SetActive (true);
				ui.transform.Find ("bt_exit").gameObject.SetActive (true);
			} else {
				MusicController.GetInstance ().resume ();
				GlobalVariables.pause = false;
				Time.timeScale = 1;
				ui.transform.Find ("mask").gameObject.SetActive (false);
				ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (0f);
				ui.transform.Find ("bt_resume").gameObject.SetActive (false);
				ui.transform.Find ("bt_menu").gameObject.SetActive (false);
				ui.transform.Find ("bt_exit").gameObject.SetActive (false);
			}
		}
	}

	public void resume_game(){
		MusicController.GetInstance ().resume ();
		GlobalVariables.pause = false;
		Time.timeScale = 1;
		ui.transform.Find ("mask").gameObject.SetActive (false);
		ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (0f);
		ui.transform.Find ("bt_resume").gameObject.SetActive (false);
		ui.transform.Find ("bt_menu").gameObject.SetActive (false);
		ui.transform.Find ("bt_exit").gameObject.SetActive (false);
		#if UNITY_IPHONE || UNITY_ANDROID
		GlobalVariables.resumeButtonPushed = true;
		#endif
	}

	public void return_to_main(){
		MusicController.GetInstance ().playMusic (0);
		SaveLoad sl = new SaveLoad ();
		sl.save ();
		Time.timeScale = 1;
		SceneManager.LoadScene ("main_menu");
	}

	public void exit_game(){
		SaveLoad sl = new SaveLoad ();
		sl.save ();
		Application.Quit ();
	}
}
