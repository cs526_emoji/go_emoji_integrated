﻿using UnityEngine;
using System.Collections;

public class YellowBallController : MonoBehaviour {

	private int deathCount;
	private bool dead;

	void Start () {
		deathCount = 100;
		dead = false;
		GetComponent<CircleCollider2D> ().enabled = true;
	}
	

	void Update () {
		if (deathCount > 0) {
			deathCount--;
			if (deathCount == 0) {
				dead = false;
				gameObject.SetActive (false);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("poop")&&!dead)
		{
			other.GetComponent<PoopHealth> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("pig")&&!dead)
		{
			other.GetComponent<PigHealth> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("wood")&&!dead)
		{
			other.GetComponent<WoodHealth> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("fire_thrower")&&!dead)
		{
			other.GetComponent<FireThrowerController> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("Zombie")&&!dead)
		{
			other.GetComponent<Zombie> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("Golem")&&!dead)
		{
			other.GetComponent<Golem> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("mouse")&&!dead)
		{
			other.GetComponent<MouseController> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("bug")&&!dead)
		{
			other.GetComponent<BugController> ().takeDmg (GlobalVariables.shopLevels[5]*5);
			GlobalVariables.damageDealt += GlobalVariables.shopLevels [5] * 5;
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("bugBall")&&!dead)
		{
			deathCount = 5;
			dead = true;
		}
		else if (other.gameObject.CompareTag("coder")&&!dead)
		{
			deathCount = 5;
			dead = true;
		}
		else if(other.gameObject.CompareTag("final_wall")){
			if (transform.position.y < -5.86f||transform.position.y > 3.3f) {
				GetComponent<CircleCollider2D> ().enabled = false;
			} else {
				GetComponent<CircleCollider2D> ().enabled = true;
			}
		}
	}

	public void reset(){
		Start ();
	}
}
