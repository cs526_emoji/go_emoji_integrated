﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundButtonActions : MonoBehaviour {

	public GameObject ui;
	public Sprite sound_off;
	public Sprite sound_on;
	public Sprite music_off;
	public Sprite music_on;

	void Start(){
		ui.transform.FindChild ("bt_scores").GetComponent<Image> ().sprite = GlobalVariables.sound ? sound_on : sound_off;
		ui.transform.FindChild ("bt_medals").GetComponent<Image> ().sprite = GlobalVariables.music ? music_on : music_off;
	}

	public void sound(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
			ui.transform.FindChild ("bt_scores").GetComponent<Image> ().sprite = sound_off;
		} else {
			ui.transform.FindChild ("bt_scores").GetComponent<Image> ().sprite = sound_on;
		}
		GlobalVariables.sound = !GlobalVariables.sound;
	}

	public void music(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		if (GlobalVariables.music) {
			ui.transform.FindChild ("bt_medals").GetComponent<Image> ().sprite = music_off;
			MusicController.GetInstance ().musicOff ();
		} else {
			ui.transform.FindChild ("bt_medals").GetComponent<Image> ().sprite = music_on;
			MusicController.GetInstance ().musicOn ();
		}
		GlobalVariables.music = !GlobalVariables.music;
	}
}
