﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuActions : MonoBehaviour {

	private int timer;

	void Start () {
		if (!GlobalVariables.applicationStarted) {
			SaveLoad sl = new SaveLoad ();
			sl.load ();
			if (!GlobalVariables.music) {
				MusicController.GetInstance ().transform.GetComponent<AudioSource> ().volume = 0.0f;
			}
			GlobalVariables.applicationStarted = true;
		}
		timer = 0;
		transform.Find ("mask").gameObject.SetActive (true);
		transform.Find ("logo").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_start").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_survival").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_scores").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_medals").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_credits").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
		transform.Find ("bt_exit").GetComponent<CanvasRenderer> ().SetColor(new Color (1f, 1f, 1f, 0f));
	}

	void Update () {
		if (timer < 300) {
			setAlpha (0, 100, 0, timer, "mask");
			setAlpha (80, 150, 1, timer, "logo");
			setAlpha (100, 150, 1, timer, "bt_start");
			setPositionX (100, 150, 20, timer, "bt_start");
			setAlpha (110, 160, 1, timer, "bt_survival");
			setPositionX (110, 160, -20, timer, "bt_survival");
			setAlpha (120, 170, 1, timer, "bt_scores");
			setPositionX (120, 170, 20, timer, "bt_scores");
			setAlpha (130, 180, 1, timer, "bt_medals");
			setPositionX (130, 180, -20, timer, "bt_medals");
			setAlpha (140, 190, 1, timer, "bt_credits");
			setPositionX (140, 190, 20, timer, "bt_credits");
			setAlpha (150, 200, 1, timer, "bt_exit");
			setPositionX (150, 200, -20, timer, "bt_exit");
			emojiAnimation ();
			timer++;
		}
		if (timer == 100) {
			transform.Find ("mask").gameObject.SetActive (false);
		}
		if (GlobalVariables.startFadeIn) {
			if (GlobalVariables.music)
				MusicController.GetInstance ().fadeIn (0.04f);
			else
				GlobalVariables.startFadeIn = false;
		}
		if (GlobalVariables.startFadeOut) {
			if (!GlobalVariables.music)
				MusicController.GetInstance ().fadeOut (0.04f);
			else
				GlobalVariables.startFadeOut = false;
		}
	}

	void setAlpha(int min, int max, int mode, int curTime, string name){
		if (curTime >= min && curTime <= max) {
			float alpha;
			if (mode == 0) {
				//alpha down
				alpha = (float)(max - curTime) / (float)(max - min) * 1.0f;
			} else {
				//alpha up
				alpha = (float)(curTime - min) / (float)(max - min) * 1.0f;
			}
			transform.Find (name).GetComponent<CanvasRenderer> ().SetColor (new Color (1f, 1f, 1f, alpha));
		}
	}

	void setPositionX(int min, int max, float transX, int curTime, string name){
		if (curTime >= min && curTime <= max) {
			float pos = (float)(max - curTime) / (float)(max - min) * transX * 0.03f;
			Vector3 oldPos = transform.Find (name).transform.position;
			transform.Find (name).transform.position = new Vector3(oldPos.x + pos,oldPos.y,oldPos.z);
		}
	}

	void emojiAnimation(){
		if (timer >= 200 && timer <= 220) {
			float speed = 5.5f * (Screen.width / 1024.0f);
			Vector3 oldPos = transform.Find ("emoji").transform.position;
			transform.Find ("emoji").transform.position = new Vector3 (oldPos.x, oldPos.y - speed, oldPos.z);
		} else if (timer >= 220 && timer <= 250) {
			float speed = 0.93f * (Screen.width / 1024.0f);
			Vector3 oldPos = transform.Find ("emoji").transform.position;
			transform.Find ("emoji").transform.position = new Vector3 (oldPos.x - speed, oldPos.y, oldPos.z);
		} else if (timer >= 250 && timer <= 280) {
			float speedX = 0.8f * (Screen.width / 1024.0f);
			float speedY = 0.22f * (Screen.width / 1024.0f);
			float speedR = 1.5f;
			Vector3 oldPos = transform.Find ("emoji").transform.position;
			transform.Find ("emoji").transform.position = new Vector3 (oldPos.x - speedX, oldPos.y - speedY, oldPos.z);
			transform.Find ("emoji").transform.Rotate (new Vector3 (0, 0, speedR));
		}
	}
}
