﻿using UnityEngine;
using System.Collections;

public class YellowBall3DController : MonoBehaviour {

	private int deathCount;

	void Start () {
		deathCount = 100;
	}


	void Update () {
		if (deathCount > 0) {
			deathCount--;
			if (deathCount == 0) {
				gameObject.SetActive (false);
			}
		}
	}

	public void reset(){
		Start ();
	}
}
