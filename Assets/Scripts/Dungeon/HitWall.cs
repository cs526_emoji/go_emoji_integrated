﻿using UnityEngine;
using System.Collections;

public class HitWall : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("wall"))
		{
			gameObject.SetActive(false);
		}
		else if (other.gameObject.CompareTag("door"))
		{
			gameObject.SetActive(false);
		}
		else if (other.gameObject.CompareTag("mid_door"))
		{
			gameObject.SetActive(false);
		}
		else if (other.gameObject.CompareTag("final_door"))
		{
			gameObject.SetActive(false);
		}
	}
}
