﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject player; 

	private float offsetX;
	private float offsetY;
	public float minX;
	public float minY;

	void Start ()
	{
		offsetX = (transform.position.x - player.transform.position.x)/2;
		offsetY = (transform.position.y - player.transform.position.y)/2;
	}

	void LateUpdate ()
	{
		Vector3 pos = transform.position;
		if (transform.position.x - player.transform.position.x > offsetX) {
			pos.x = player.transform.position.x + offsetX;
		}
		else if (transform.position.x - player.transform.position.x < -offsetX) {
			pos.x = player.transform.position.x - offsetX;
		}
		if (transform.position.y - player.transform.position.y > offsetY) {
			pos.y = player.transform.position.y + offsetY;
		}
		else if (transform.position.y - player.transform.position.y < -offsetY) {
			pos.y = player.transform.position.y - offsetY;
		}
		if (pos.x < minX) {
			pos.x = minX;
		}
		else if (pos.x > -minX) {
			pos.x = -minX;
		}
		if (pos.y < minY) {
			pos.y = minY;
		}
		else if (pos.y > -minY) {
			pos.y = -minY;
		}
		transform.position = pos;
	}
}
