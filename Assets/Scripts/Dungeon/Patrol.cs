﻿using UnityEngine;
using System.Collections;

public class Patrol : MonoBehaviour {

	public Vector2 target;
	private Vector2 start;
	private Vector2 curTarget;
	private bool flag;
	private Rigidbody2D rb2d;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		start.x = transform.position.x;
		start.y = transform.position.y;
		curTarget = target;
		flag = true;
	}

	void Update () {
		if (reach ()) {
			if (flag) {
				curTarget = start;
			}
			else {
				curTarget = target;
			}
			flag = !flag;
		}
	}

	void FixedUpdate () {
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		float moveH = (curTarget.x - transform.position.x);
		float moveV = (curTarget.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		movement.Normalize ();
		if(rb2d.velocity.magnitude<0.5)
			rb2d.AddForce (movement);
	}

	bool reach() {
		return (Mathf.Abs(transform.position.x - curTarget.x)<0.1)&&(Mathf.Abs(transform.position.y - curTarget.y)<0.1);
	}
}
