﻿using UnityEngine;
using System.Collections;

public class Patrol3Points : MonoBehaviour {

	public Vector2 target1;
	public Vector2 target2;
	private Vector2 start;
	private Vector2 curTarget;
	private int count;
	private Rigidbody2D rb2d;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		start.x = transform.position.x;
		start.y = transform.position.y;
		curTarget = target1;
		count = 0;
	}

	void Update () {
		if (reach ()) {
			if (count == 0) {
				curTarget = target2;
			} else if (count == 1) {
				curTarget = start;
			} else {
				curTarget = target1;
			}
			count = (count + 1) % 3;
		}
	}

	void FixedUpdate () {
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		float moveH = (curTarget.x - transform.position.x);
		float moveV = (curTarget.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		movement.Normalize ();
		if(rb2d.velocity.magnitude<0.5)
			rb2d.AddForce (movement);
	}

	bool reach() {
		return (Mathf.Abs(transform.position.x - curTarget.x)<0.1)&&(Mathf.Abs(transform.position.y - curTarget.y)<0.1);
	}
}
