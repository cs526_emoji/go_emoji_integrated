﻿using UnityEngine;
using System.Collections;

public class PigHealth : MonoBehaviour {

	public Player player;
	public GameObject shoot;
	public GameObject shoot1;
	public GameObject shoot2;
	public GameObject fire;
	public GameObject fire1;
	public GameObject key;

	public GameObject hb;
	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;

	private bool isDie;
	private int dieCount;

	void Start () {
		timer = 0;
		maxhp = 300;
		hp = 300;
		oldhp = 300;
		healthChange = false;
		isDie = false;
		dieCount = 0;
	}

	void Update() {
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (isDie) {
			dieCount++;
			if (dieCount == 20) {
				gameObject.SetActive(false);
			}
		}
	}

	public void takeDmg(int val){
		if (isDie) {
			return;
		}
		if (hp > val) {
			timer = 0;
			hp -= val;
			updateHB ();
		} else {
			die();
		}
	}

	void die(){
		isDie = true;
		hb.SetActive (false);
		GetComponent<CircleCollider2D> ().enabled = false;
		GetComponent<SpriteRenderer> ().color = new Vector4 (1f,1f,1f,0f);
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().Play ();
		}
		key.SetActive (true);
		key.transform.position = transform.position;
		if (shoot.activeInHierarchy&&shoot.GetComponent<Rigidbody2D> ().velocity.magnitude < 0.1) {
			shoot.SetActive (false);
			shoot1.SetActive (false);
			shoot2.SetActive (false);
		}
		if (fire.activeInHierarchy&&fire.GetComponent<Rigidbody2D> ().velocity.magnitude < 0.1) {
			fire.SetActive (false);
			fire1.SetActive (false);
		}
		GetComponent<Pig> ().enabled = false;
		int n = Random.Range (40, 60);
		n = (int)(n * GlobalVariables.boost);
		player.getMoney (n);
		GlobalVariables.enemyKilled++;
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.5f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 45f * 0.01f;
		float scale = (float)hp / maxhp * 0.15f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.1f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 45f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.15f;
		hb.transform.position = transform.position + Vector3.up * 0.5f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.1f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.5f;
	}
}
