﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {

	private int count;
	private int rot;

	void Start() {
		transform.Rotate (new Vector3 (0, 0, -45));
		count = 50;
		rot = 45;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (count == 0) {
			count = 50;
			rot = -rot;
		}
		transform.Rotate (new Vector3 (0, 0, rot) * Time.deltaTime);
		count--;
	}
}
