﻿using UnityEngine;
using System.Collections;

public class Pig : MonoBehaviour {

	public GameObject shoot1;
	public GameObject shoot2;
	public GameObject shoot3;
	public GameObject fire1;
	public GameObject fire2;
	public GameObject player;
	private int timer1;
	private int timer2;

	void Start () {
		Random.seed = (int)(transform.position.x*100);
		int n = Random.Range (0, 50);
		int m = Random.Range (50, 100);
		timer1 = 100 + n;
		timer2 = 100 + m;
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		faceToPlayer ();
		UpdateShoot ();
		UpdateFire ();
	}

	void faceToPlayer (){
		Vector3 vectorToTarget = player.transform.position - transform.position;
		float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + 90;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation, q, Time.deltaTime);
	}

	void UpdateShoot(){
		if (timer1 > 90) {
			timer1--;
		} else if (timer1 == 90) {
			shoot1.SetActive (true);
			shoot2.SetActive (true);
			shoot3.SetActive (true);
			setPosAndRot ();
			shoot1.transform.localScale = new Vector3 (0.01f, 0.01f, 1);
			shoot2.transform.localScale = new Vector3 (0.01f, 0.01f, 1);
			shoot3.transform.localScale = new Vector3 (0.01f, 0.01f, 1);
			timer1--;
		} else if (timer1 > 0) {
			setPosAndRot ();
			shoot1.transform.localScale += new Vector3 (0.001f, 0.001f, 0);
			shoot2.transform.localScale += new Vector3 (0.001f, 0.001f, 0);
			shoot3.transform.localScale += new Vector3 (0.001f, 0.001f, 0);
			timer1--;
		} else if (timer1 == 0) {
			Vector2 movement1 = new Vector2 (shoot1.transform.up.x, shoot1.transform.up.y);
			Vector2 movement2 = new Vector2 (shoot2.transform.up.x, shoot2.transform.up.y);
			Vector2 movement3 = new Vector2 (shoot3.transform.up.x, shoot3.transform.up.y);
			movement1.Normalize ();
			movement2.Normalize ();
			movement3.Normalize ();
			shoot1.GetComponent<Rigidbody2D> ().velocity = -movement1;
			shoot2.GetComponent<Rigidbody2D> ().velocity = -movement2;
			shoot3.GetComponent<Rigidbody2D> ().velocity = -movement3;
			timer1--;
		} else if (!shoot1.activeSelf&&!shoot2.activeSelf&&!shoot3.activeSelf){
			Random.seed = (int)(transform.position.x*100);
			int n = Random.Range (0, 50);
			timer1 = 100 + n;
		}
	}

	void UpdateFire (){
		if (timer2 > 45) {
			timer2--;
		} else if (timer2 == 45) {
			fire1.SetActive (true);
			fire2.SetActive (true);
			setPosAndRot1 ();
			fire1.transform.localScale = new Vector3 (0.1f, 0.01f, 1);
			fire2.transform.localScale = new Vector3 (0.1f, 0.01f, 1);
			timer2--;
		} else if (timer2 > 0) {
			setPosAndRot1 ();
			timer2--;
		} else if (timer2 == 0) {
			Vector2 movement1 = new Vector2 (fire1.transform.up.x, fire1.transform.up.y);
			Vector2 movement2 = new Vector2 (fire2.transform.up.x, fire2.transform.up.y);
			movement1.Normalize ();
			movement2.Normalize ();
			fire1.GetComponent<Rigidbody2D> ().velocity = movement1 * -1.5f;
			fire2.GetComponent<Rigidbody2D> ().velocity = movement2 * -1.5f;
			timer2--;
		} else if (timer2 > -45) {
			fire1.transform.localScale += new Vector3 (0, 0.002f, 0);
			fire2.transform.localScale += new Vector3 (0, 0.002f, 0);
			timer2--;
		} else if (!fire1.activeSelf&&!fire2.activeSelf){
			Random.seed = (int)(transform.position.x*100);
			int m = Random.Range (50, 100);
			timer2 = 100 + m;
		}
	}

	void setPosAndRot (){
		Vector3 offset = new Vector3 (transform.up.x, transform.up.y, 0f);
		offset.Normalize ();
		shoot1.transform.position = transform.position - offset * 0.15f;
		shoot2.transform.position = transform.position - offset * 0.15f;
		shoot3.transform.position = transform.position - offset * 0.15f;
		Vector3 rot = transform.rotation.eulerAngles;
		shoot1.transform.rotation = transform.rotation;
		shoot2.transform.rotation = Quaternion.Euler(new Vector3(rot.x,rot.y,rot.z+15));
		shoot3.transform.rotation = Quaternion.Euler(new Vector3(rot.x,rot.y,rot.z-15));
	}

	void setPosAndRot1 (){
		Vector3 offset = new Vector3 (transform.up.x, transform.up.y, 0f);
		offset.Normalize ();
		fire1.transform.position = transform.position - offset * 0.15f;
		fire2.transform.position = transform.position - offset * 0.15f;
		Vector3 rot = transform.rotation.eulerAngles;
		fire1.transform.rotation = Quaternion.Euler(new Vector3(rot.x,rot.y,rot.z+10));
		fire2.transform.rotation = Quaternion.Euler(new Vector3(rot.x,rot.y,rot.z-10));
	}
}
