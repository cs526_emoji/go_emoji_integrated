﻿using UnityEngine;
using System.Collections;

public class EvilCameraController : MonoBehaviour {

	public GameObject player; 

	private float offsetY;
	private float startY;

	void LateUpdate ()
	{
		Vector3 pos = transform.position;
		pos.y = player.transform.position.y + offsetY;
		if (pos.y < startY) {
			pos.y = startY;
		}
		if (pos.y > 7.93f) {
			pos.y = 7.93f;
		}
		transform.position = pos;
	}

	public void getOffset(){
		startY = transform.position.y;
		offsetY = transform.position.y - player.transform.position.y;
	}
}
