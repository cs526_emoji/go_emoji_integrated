﻿using UnityEngine;
using System.Collections;

public class BugController : MonoBehaviour {

	public GameObject player;
	public GameObject wayPoints;
	private Vector3 target;
	private int turningCount;
	private int movingCount;
	private Rigidbody2D rb2d;


	public GameObject hb;
	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;
	private bool dead;
	private int deathCount;

	public GameObject ball;
	private int appear;

	void Start () {
		GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.0f);
		Random.seed = System.Environment.TickCount;
		turningCount = 50;
		movingCount = 0;
		rb2d = GetComponent<Rigidbody2D> ();
		chooseTarget ();

		timer = 0;
		maxhp = 50;
		hp = 50;
		oldhp = 50;
		healthChange = false;
		dead = false;
		deathCount = 0;
		appear = 0;
	}

	void Update() {
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (appear < 50) {
			appear++;
			GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, appear * 0.02f);
		}
		if (deathCount > 0) {
			deathCount--;
			if (deathCount == 0) {
				hb.SetActive (false);
				ball.SetActive (true);
				ball.transform.position = transform.position;
				int n = Random.Range (15, 25);
				n = (int)(n * GlobalVariables.boost);
				player.GetComponent<HackerPlayerController> ().getMoney (n);
				if (GlobalVariables.sound) {
					player.GetComponent<AudioSource> ().PlayOneShot (player.GetComponent<HackerPlayerController>(). moneySound, 1.0f);
				}
				GlobalVariables.enemyKilled++;
				gameObject.SetActive(false);
			}
		}
	}

	void FixedUpdate(){
		if (dead) {
			return;
		}
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		if (turningCount > 0) {
			faceToTarget ();
			turningCount--;
			if (turningCount == 0) {
				movingCount = 100;
			}
		} else {
			moveToTarget ();
			movingCount--;
			if (reach ()||movingCount==0) {
				chooseTarget ();
				rb2d.velocity = new Vector2 (0f,0f);
				turningCount = 50;
			}
		}
	}

	void chooseTarget(){
		int n = Random.Range (0, 31);
		target = wayPoints.transform.GetChild (n).transform.position;
	}

	void faceToTarget(){
		Vector3 vectorToTarget = target - transform.position;
		float step = Vector3.Angle (vectorToTarget, transform.up) / turningCount;
		transform.RotateAround (transform.position, transform.forward, step);
	}

	void moveToTarget(){
		float moveH = (target.x - transform.position.x);
		float moveV = (target.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		Vector2 resistance = -rb2d.velocity;
		movement.Normalize ();
		resistance.Normalize ();
		rb2d.AddForce (resistance * rb2d.velocity.magnitude * 10f);
		rb2d.AddForce (movement * 20f);
		rb2d.angularVelocity = 0;
	}

	bool reach(){
		return Vector3.Distance (target, transform.position) < 0.01f;
	}

	public void takeDmg(int val){
		if (dead) {
			return;
		}
		timer = 0;
		if (hp > val) {
			hp -= val;
		} else {
			hp = 0;
			die();
		}
		updateHB ();
	}

	void die(){
		dead = true;
		deathCount = 30;
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 22.5f * 0.01f;
		float scale = (float)hp / maxhp * 0.075f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 22.5f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.075f;
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.3f;
	}

	public void reset(){
		int n = Random.Range (0, 31);
		transform.position = wayPoints.transform.GetChild (n).transform.position;
		Start ();
	}
}
