﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HackerPlayerController : MonoBehaviour {

	public AudioClip equipSound;
	public AudioClip clickSound;
	public AudioClip moneySound;
	public AudioClip emojiDie;
	public AudioClip potionSound;
	public AudioClip bulletSound;
	public AudioClip freezeSound;
	public AudioClip laugh;

	public GameObject ui;
	public GameObject blockEntry;
	public GameObject main_camera;
	public GameObject doorBlocks;
    public GameObject cpp;
	public GameObject mouses;

	private Rigidbody2D rb2d;
	private bool healthChange;
	private Vector3 oldPos;

	private int hp;
	private int oldhp;
	private int maxhp;
	private int damage;

	private int timer;
	private int resetCount;
	private int state; //normal = 0, die = 1, win = 2
	private bool flag_getMoney;

	private bool controllable;
	private int count;
	private int mouseKilled;

	public GameObject shootPoints;
	private int wordCount;
	private bool flagGetWord;
	private GameObject curWord;

	public GameObject coder;
	private int stunCount;
	private Sprite original;
	public Sprite emojiStun;

	private int potionCount;
	private int potionPow;
	private float freezeTime;
	private float curFreezeTime;
	private int freezeCount;
	private int freezeMaskTimer;
	private int cannonCount;
	private int cannonPow;
	private int cannonDelay;
	private int cannonIdx;
	public Camera cam;
	public GameObject yellowBalls;
    

	void Start () {
		if (!GlobalVariables.sceneStarted) {
			MusicController.GetInstance ().playMusic (7);
			GlobalVariables.sceneStarted = true;
		}
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		GlobalVariables.pause = false;
		ui.transform.Find ("mask").gameObject.SetActive (true);
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		timer = 25;
		resetCount = 0;
		state = 0;
		rb2d = GetComponent<Rigidbody2D> ();
		original = gameObject.GetComponent<SpriteRenderer> ().sprite;
		maxhp = GlobalVariables.maxHP;
		hp = maxhp;
		oldhp = maxhp;
		damage = GlobalVariables.damage;
		healthChange = false;
		oldPos = ui.transform.Find ("hb_main").transform.position;
		flag_getMoney = false;
		controllable = false;
		count = 0;
		Random.seed = System.Environment.TickCount;
		wordCount = 0;
		flagGetWord = false;
		stunCount = 0;
		mouseKilled = 0;

		initActiveSkills ();

		#if UNITY_IPHONE || UNITY_ANDROID
		GlobalVariables.resumeButtonPushed = false;
		#endif
	}

	void Update() {
		if (timer > 0) {
			timer--;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (timer*4*0.01f);
			if (timer == 0) {
				ui.transform.Find ("mask").gameObject.SetActive (false);
			}
		}
		if (healthChange) {
			updateHBFrame ();
		}
		if (state > 0) {
			ui.transform.Find ("mask").gameObject.SetActive (true);
			resetCount++;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (resetCount*4*0.01f);
			if (resetCount == 25) {
				if (state == 1) {
					SceneManager.LoadScene ("stage7");
				} else {
					if (GlobalVariables.currentLevel == 6) {
						GlobalVariables.currentLevel = 7;
					}
					saveGame ();
					MusicController.GetInstance ().playMusic (0);
					SceneManager.LoadScene ("world_map");
				}
			}
		}
		if (flag_getMoney&&ui.transform.Find("moneyText").transform.localScale.x>0.5) {
			ui.transform.Find ("moneyText").transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f);
			if (ui.transform.Find ("moneyText").transform.localScale.x == 0.5f) {
				flag_getMoney = false;
			}
		}
		if (flagGetWord&&curWord.transform.localScale.x>0.4) {
			curWord.transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f);
			if (curWord.transform.localScale.x == 0.5f) {
				flagGetWord = false;
			}
		}
		if (count > 0) {
			Vector3 cPos = main_camera.transform.position;
			cPos.y += 0.075f * count;
			main_camera.transform.position = cPos;
			count--;
			if (count == 0) {
				controllable = true;
				doorBlocks.transform.GetChild (0).gameObject.SetActive (true);
				doorBlocks.transform.GetChild (1).gameObject.SetActive (true);
				doorBlocks.transform.GetChild (2).gameObject.SetActive (true);
				if (transform.position.y > -1.0f) {
					mouses.SetActive (true);
				}
				if (transform.position.y > 3f) {
					main_camera.GetComponent<EvilCameraController> ().enabled = true;
					main_camera.GetComponent<EvilCameraController> ().getOffset ();
					coder.GetComponent<coderController> ().enabled = true;
					MusicController.GetInstance ().musicOff ();
					GlobalVariables.sceneStarted = false;
					if (GlobalVariables.sound) {
						GetComponent<AudioSource> ().PlayOneShot (laugh, 1.0f);
					}
				}
			}
		}
		shootCannon ();
	}

	void FixedUpdate () {
		#if UNITY_IPHONE || UNITY_ANDROID
		float moveHorizontal = Input.acceleration.x;
		float moveVertical = Input.acceleration.y;
		#else
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		#endif

		if (!controllable) {
			moveHorizontal = 0;
			moveVertical = 0.3f;
			if (stunCount>0) {
				moveVertical = 0;
				stunCount--;
				if (stunCount == 0) {
					controllable = true;
					gameObject.GetComponent<SpriteRenderer> ().sprite = original;
				}
			}
		}

		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);
		Vector2 resistance = -rb2d.velocity;
		resistance.Normalize ();
		rb2d.AddForce (resistance * rb2d.velocity.magnitude * 5f);
		rb2d.AddForce (movement * 30f);
	}

	void LateUpdate (){
		if (GlobalVariables.freeze) {
			curFreezeTime += Time.deltaTime;
			if (curFreezeTime > freezeTime) {
				GlobalVariables.freeze = false;
				freezeMaskTimer = 50;
			}
		}
		if (freezeMaskTimer > 0) {
			freezeMaskTimer--;
			ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (freezeMaskTimer*0.006f);
			if (freezeMaskTimer == 0) {
				ui.transform.Find ("freezeMask").gameObject.SetActive (false);
			}
		}
		if (GlobalVariables.startFadeIn) {
			MusicController.GetInstance ().fadeIn (0.04f);
		}
		if (GlobalVariables.startFadeOut) {
			MusicController.GetInstance ().fadeOut (0.04f);
		}
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.gameObject.CompareTag("cpp"))
		{
			other.gameObject.SetActive(false);
			doorBlocks.transform.GetChild (2).gameObject.SetActive (false);
			shootPoints.SetActive (true);
			if (GlobalVariables.sound) {
				GetComponent<AudioSource> ().PlayOneShot (equipSound, 1.0f);
			}

		}
		else if (other.gameObject.CompareTag("blockEntry"))
		{
			other.gameObject.SetActive(false);
			blockEntry.SetActive (true);
			controllable = true;
		}
		else if (other.gameObject.CompareTag("finalDoor1"))
		{
			other.gameObject.SetActive(false);
			controllable = false;
			count = 10;
		}
		else if (other.gameObject.CompareTag("c_error")&&!GlobalVariables.freeze)
        {
			GlobalVariables.damageTaken += 20;
			other.gameObject.SetActive(false);
			if (hp > 20) {
				hp -= 20;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
        }
		else if (other.gameObject.CompareTag("shoot")&&!GlobalVariables.freeze)
		{
			GlobalVariables.damageTaken += 5;
			other.gameObject.SetActive(false);
			if (hp > 5) {
				hp -= 5;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
		}
		else if (other.gameObject.CompareTag("cpu")&&!GlobalVariables.freeze)
		{
			GlobalVariables.damageTaken += 15;
			other.gameObject.SetActive(false);
			if (hp > 15) {
				hp -= 15;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
				stunCount = 60;
				controllable = false;
				gameObject.GetComponent<SpriteRenderer> ().sprite = emojiStun;
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
		}
		else if (other.gameObject.CompareTag("hardware")&&!GlobalVariables.freeze)
		{
			GlobalVariables.damageTaken += 40;
			other.gameObject.SetActive(false);
			if (hp > 40) {
				hp -= 40;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
		}
		else if (other.gameObject.CompareTag("bug"))
		{
			Vector2 relativeSpeed = other.GetComponent<Rigidbody2D> ().velocity - rb2d.velocity;
			if (relativeSpeed.magnitude > 1.0f) {
				other.GetComponent<BugController> ().takeDmg (damage);
				GlobalVariables.damageDealt += damage;
			}
		}
		else if (other.gameObject.CompareTag("mouse"))
		{
			Vector2 relativeSpeed = other.GetComponent<Rigidbody2D> ().velocity - rb2d.velocity;
			if (relativeSpeed.magnitude > 1.0f) {
				other.GetComponent<MouseController> ().takeDmg (damage);
				GlobalVariables.damageDealt += damage;
			}
		}
        else if (other.gameObject.CompareTag("c_code"))
        {
			other.gameObject.SetActive(false);
			if (flagGetWord) {
				curWord.transform.localScale = new Vector3 (0.4f, 0.4f, 1.0f);
			}
			curWord = ui.transform.Find ("JavaCode").transform.Find (other.gameObject.name).gameObject;
			if (!curWord.activeSelf) {
				curWord.SetActive (true);
				wordCount++;
				if (wordCount == 5) {
					doorBlocks.transform.GetChild (1).gameObject.SetActive (false);
					foreach (Transform child in shootPoints.transform) {
						child.GetComponent<ShootPointController> ().selfDestroy ();
					}
				}
			}
			curWord.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			flagGetWord = true;
			if (GlobalVariables.sound) {
				GetComponent<AudioSource> ().PlayOneShot (clickSound, 1.0f);
			}
        }
		else if (other.gameObject.CompareTag("Finish"))
		{
			win ();
		}
	}

	void OnTriggerStay2D(Collider2D other){
		if (other.gameObject.CompareTag("wire")&&!GlobalVariables.freeze)
		{
			GlobalVariables.damageTaken += 5;
			if (hp > 5) {
				hp -= 5;
				updateHB ();
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().Play ();
				}
			} else {
				hp = 0;
				updateHB ();
				die ();
			}
		}
	}

	void die(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (emojiDie, 1.0f);
		}
		if (!GlobalVariables.sceneStarted) {
			MusicController.GetInstance ().musicOff ();
		}
		GlobalVariables.deathCount++;
		state = 1;
	}

	void win(){
		MusicController.GetInstance ().musicOff ();
		GlobalVariables.winCount++;
		state = 2;
	}

	void updateHB(){
		float dist = (1.0f - (float)hp / maxhp) * 60f * (Screen.width / 800.0f);
		float scale = (float)hp / maxhp * 0.2f;
		ui.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.2f, 1f);
		ui.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		if (oldhp > hp) {
			healthChange = true;
		} else if (oldhp < hp) {
			oldhp = hp;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	void updateHBFrame(){
		if (oldhp == hp) {
			healthChange = false;
		} else {
			oldhp--;
			float dist = (1.0f - (float)oldhp / maxhp) * 60f * (Screen.width / 800.0f);
			float scale = (float)oldhp / maxhp * 0.2f;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	public void getMoney(int val){
		GlobalVariables.moneyEarned += val;
		GlobalVariables.money += val;
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		ui.transform.Find ("moneyText").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		flag_getMoney = true;
	}

	public void killedMouse(){
		mouseKilled++;
		if (mouseKilled == 4) {
			doorBlocks.transform.GetChild (2).gameObject.SetActive (false);
		}
	}

	void initActiveSkills(){
		GlobalVariables.freeze = false;
		potionCount = GlobalVariables.shopLevels [4];
		potionPow = GlobalVariables.potionPower;
		freezeTime = GlobalVariables.shopLevels [3] * 0.8f;
		curFreezeTime = 0.0f;
		freezeCount = GlobalVariables.shopLevels [3] > 0 ? 1 : 0;
		freezeMaskTimer = 0;
		cannonCount = GlobalVariables.shopLevels [5] > 0 ? 10 + 3 * GlobalVariables.shopLevels [5] : 0;
		cannonPow = GlobalVariables.shopLevels [5] * 5;
		cannonDelay = 0;
		cannonIdx = 0;
		if (potionCount > 0) {
			ui.transform.Find ("potion").gameObject.SetActive (true);
			ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		}
		if (freezeCount > 0) {
			ui.transform.Find ("hourglass").gameObject.SetActive (true);
			ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		}
		if (cannonCount > 0) {
			ui.transform.Find ("cannon").gameObject.SetActive (true);
			ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
		}
	}

	public void usePotion(){
		if (potionCount == 0) {
			return;
		}
		potionCount--;
		ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		hp += potionPow;
		if (hp > maxhp) {
			hp = maxhp;
		}
		updateHB ();
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (potionSound, 1.0f);
		}
	}

	public void useFreeze(){
		if (freezeCount == 0) {
			return;
		}
		freezeCount--;
		ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		ui.transform.Find ("freezeMask").gameObject.SetActive (true);
		ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (0.3f);
		GlobalVariables.freeze = true;
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (freezeSound, 1.0f);
		}
	}

	void shootCannon(){
		if (cannonDelay > 0) {
			cannonDelay--;
		}
		else if (!GlobalVariables.pause && cannonPow > 0 && cannonCount > 0) {
			#if UNITY_IPHONE || UNITY_ANDROID
			if (Input.touchCount > 0) {
				if(GlobalVariables.resumeButtonPushed){
					GlobalVariables.resumeButtonPushed = false;
					return;
				}
				Vector2 mp = Input.GetTouch (0).position;
				Vector3 wp = cam.ScreenToWorldPoint (new Vector3(mp.x,mp.y,0));
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.z = 0;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBallController>().reset();
				cur.transform.position = transform.position;
				cur.GetComponent<Rigidbody2D>().velocity = dir * 5.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#else
			if (Input.GetButtonDown ("Fire1")) {
				Vector3 mp = Input.mousePosition;
				Vector3 wp = cam.ScreenToWorldPoint (mp);
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.z = 0;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBallController>().reset();
				cur.transform.position = transform.position;
				cur.GetComponent<Rigidbody2D>().velocity = dir * 5.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#endif
		}
	}

	bool inButtonArea(Vector3 worldPoint){
		RectTransform CanvasRect = ui.GetComponent<RectTransform>();
		float canvasWidth = CanvasRect.sizeDelta.x;
		float canvasHeight = CanvasRect.sizeDelta.y;
		Vector3 viewPoint = cam.WorldToViewportPoint (worldPoint);
		Vector2 canvasPoint = new Vector2 (viewPoint.x * canvasWidth - canvasWidth * 0.5f, viewPoint.y * canvasHeight - canvasHeight * 0.5f);
		float dist;

		if (potionPow > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(100f,190f));
			if (dist < 25f)
				return true;
		}

		if (freezeTime > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(160f,190f));
			if (dist < 25f)
				return true;
		}

		dist = Vector2.Distance (canvasPoint,new Vector2(220f,190f));
		if (dist < 25f)
			return true;
		return false;

	}

	void saveGame(){
		SaveLoad sl = new SaveLoad ();
		sl.save ();
	}
}
