﻿using UnityEngine;
using System.Collections;

public class ShootPointController : MonoBehaviour {

	public GameObject shootPoints;
	public GameObject wordPool;
	private int timer;
	private GameObject target;
	private GameObject word;
	private bool flag_destroy;
	private int lifeCount;

	void Start () {
		timer = Random.Range (0, 100);
		flag_destroy = false;
		lifeCount = 0;
	}

	void Update () {
		if (GlobalVariables.pause) {
			return;
		}
		if (flag_destroy) {
			lifeCount --;
			if (word!=null&&word.activeSelf) {
				word.transform.GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.025f * lifeCount);
				if (lifeCount == 0) {
					word.SetActive (false);
				}
			}
			if (lifeCount == 0) {
				gameObject.SetActive (false);
			}
		}
		if (GlobalVariables.freeze) {
			return;
		}
		if (timer > 0) {
			timer--;
		} else if (timer == 0) {
			pickUpTarget ();
			pickUpWord ();
			shootWord ();
			timer--;
		} else if (!word.activeSelf&&!flag_destroy){
			timer = Random.Range (0, 50);
		}
	}

	void pickUpTarget(){
		do {
			int n = Random.Range (0, 14);
			target = shootPoints.transform.GetChild (n).gameObject;
		} while(sameSide (target.transform.position));
	}

	bool sameSide(Vector3 tarPos){
		Vector3 pos = transform.position;
		return ((tarPos.y == pos.y && Mathf.Abs (pos.x) < 3) || (tarPos.x == pos.x && pos.y < -1 && pos.y > -5));
	}

	void pickUpWord(){
		do {
			int n = Random.Range (0, 17);
			word = wordPool.transform.GetChild (n).gameObject;
		} while(word.activeSelf);

	}

	void shootWord(){
		word.SetActive (true);
		word.transform.position = transform.position;
		Quaternion rotation = Quaternion.LookRotation (target.transform.position - transform.position, transform.TransformDirection (Vector3.up));
		word.transform.rotation = new Quaternion (0, 0, rotation.z, rotation.w);
		float vX = target.transform.position.x - transform.position.x;
		float vY = target.transform.position.y - transform.position.y;
		Vector2 movement = new Vector2 (vX, vY);
		movement.Normalize ();
		word.GetComponent<Rigidbody2D> ().velocity = movement;
	}

	public void selfDestroy(){
		flag_destroy = true;
		lifeCount = 40;
	}
}
