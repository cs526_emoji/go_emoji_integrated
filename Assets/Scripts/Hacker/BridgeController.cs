﻿using UnityEngine;
using System.Collections;

public class BridgeController : MonoBehaviour {

	public GameObject block;
	private int count;

	void Start () {
		count = 0;
		GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.0f);
	}

	void Update () {
		if (GlobalVariables.freeze) {
			return;
		}
		if (count < 100) {
			count++;
			GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, count * 0.01f);
			if (count == 20) {
				block.SetActive (false);
			}
		}
	}
}
