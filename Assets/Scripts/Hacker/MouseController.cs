﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour {

	public GameObject player;
	private Vector3 target;
	private int turningCount;
	private int movingCount;
	private int attackCount;
	private int swingCount;
	private Rigidbody2D rb2d;


	public GameObject hb;
	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;
	private bool dead;
	private int deathCount;

	public GameObject explosion;
	private GameObject wire;
	private int state;// 0:idle 1:swing 2:turn and go to target 3&4:turn and attack
	private int appear;

	void Start () {
		GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, 0.0f);
		rb2d = GetComponent<Rigidbody2D> ();
		wire = transform.GetChild (0).gameObject;
		state = 0;
		Random.seed = System.Environment.TickCount;

		timer = 0;
		maxhp = 200;
		hp = 200;
		oldhp = 200;
		healthChange = false;
		dead = false;
		deathCount = 0;
		appear = 0;

		turningCount = 0;
		movingCount = 0;
		attackCount = 0;
		swingCount = 0;
	}

	void Update () {
		if (appear < 50) {
			appear++;
			GetComponent<SpriteRenderer> ().color = new Vector4 (1.0f, 1.0f, 1.0f, appear * 0.02f);
		}
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (GlobalVariables.freeze) {
			return;
		}
		if (!dead && state == 0) {
			state = Random.Range (1,5);
			if (state == 1) {
				GetComponent<Rotator> ().enabled = true;
				swingCount = 200;
			} else if (state == 2) {
				chooseTarget ();
				rb2d.velocity = new Vector2 (0f, 0f);
				turningCount = 50;
			} else {
				target = player.transform.position;
				rb2d.velocity = new Vector2 (0f, 0f);
				turningCount = 50;
			}
		}
		if (deathCount > 0) {
			deathCount--;
			if (deathCount == 0) {
				hb.SetActive (false);
				explosion.SetActive (true);
				explosion.transform.position = transform.position;
				if (GlobalVariables.sound) {
					explosion.GetComponent<AudioSource> ().Play ();
				}
				player.GetComponent<HackerPlayerController> ().killedMouse ();
				int n = Random.Range (60, 80);
				n = (int)(n * GlobalVariables.boost);
				player.GetComponent<HackerPlayerController> ().getMoney (n);
				GlobalVariables.enemyKilled++;
				gameObject.SetActive(false);
			}
		}
	}

	void FixedUpdate(){
		if (dead) {
			return;
		}
		if (GlobalVariables.freeze) {
			rb2d.velocity = Vector2.zero;
			rb2d.angularVelocity = 0.0f;
			return;
		}
		if (turningCount > 0) {
			faceToTarget ();
			turningCount--;
			if (turningCount == 0) {
				if (state == 2) {
					movingCount = 200;
				} else {
					wire.SetActive (true);
					attackCount = 100;
				}
			}
		} else if (movingCount > 0) {
			moveToTarget ();
			movingCount--;
			if (reach () || movingCount == 0) {
				rb2d.velocity = new Vector2 (0f, 0f);
				state = 0;
			}
		} else if (swingCount > 0) {
			swingCount--;
			if (swingCount == 0) {
				GetComponent<Rotator> ().enabled = false;
				state = 0;
			}
		} else if (attackCount > 0) {
			Vector3 scale = wire.transform.localScale;
			if (attackCount > 50) {
				scale.y = -2.0f * attackCount + 200;
			} else {
				scale.y = 2.0f * attackCount;
			}
			wire.transform.localScale = scale;
			attackCount--;
			if (attackCount == 0) {
				wire.SetActive (false);
				state = 0;
			}
		}
	}

	void chooseTarget(){
		float xPos = Random.Range (-2.93f, 2.93f);
		float yPos = Random.Range (-0.51f, 2.18f);
		target = new Vector3 (xPos, yPos, 0f);
	}

	void faceToTarget(){
		Vector3 vectorToTarget = target - transform.position;
		float step = Vector3.Angle (vectorToTarget, transform.up) / turningCount;
		transform.RotateAround (transform.position, transform.forward, step);
	}

	void moveToTarget(){
		float moveH = (target.x - transform.position.x);
		float moveV = (target.y - transform.position.y);
		Vector2 movement = new Vector2 (moveH, moveV);
		Vector2 resistance = -rb2d.velocity;
		movement.Normalize ();
		resistance.Normalize ();
		rb2d.AddForce (resistance * rb2d.velocity.magnitude * 10f);
		rb2d.AddForce (movement * 20f);
		rb2d.angularVelocity = 0;
	}

	bool reach(){
		return Vector3.Distance (target, transform.position) < 0.01f;
	}

	public void takeDmg(int val){
		if (dead) {
			return;
		}
		timer = 0;
		if (hp > val) {
			hp -= val;
		} else {
			hp = 0;
			die();
		}
		updateHB ();
	}

	void die(){
		dead = true;
		deathCount = 30;
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 22.5f * 0.01f;
		float scale = (float)hp / maxhp * 0.075f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 22.5f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.075f;
		hb.transform.position = transform.position + Vector3.up * 0.3f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.075f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.3f;
	}

}
