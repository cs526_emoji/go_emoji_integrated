﻿using UnityEngine;
using System.Collections;

public class HitShootPoint : MonoBehaviour {
	private int count = 0;

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.CompareTag("shootPoint"))
		{
			count++;
			if (count == 2) {
				count = 0;
				gameObject.SetActive (false);
			}
		}
	}


}
