﻿using UnityEngine;
using System.Collections;

public class BugBallController : MonoBehaviour {

	public GameObject bug;
	public GameObject explosion;
	private Rigidbody2D rb2d;
	private GameObject coder;
	private int delay;
	private bool flag;

	void Start () {
		rb2d = GetComponent<Rigidbody2D> ();
		delay = 0;
		flag = false;
	}

	void Update () {
		if (delay > 0) {
			delay--;
			if (delay == 0) {
				coder.GetComponent<coderController> ().takeDmg (20);
				GlobalVariables.damageDealt += 200;
				bug.SetActive(true);
				bug.GetComponent<BugController> ().reset ();
				explosion.SetActive (true);
				explosion.transform.position = transform.position;
				explosion.GetComponent<Animator> ().Play ("explosion", -1, 0f);
				if (GlobalVariables.sound) {
					explosion.GetComponent<AudioSource> ().Play ();
				}
				flag = false;
				gameObject.SetActive(false);
			}
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Player")||other.gameObject.CompareTag("yellowBall"))
		{
			rb2d.velocity = other.GetComponent<Rigidbody2D> ().velocity;
			GetComponent<CircleCollider2D> ().enabled = false;
		}
		else if (other.gameObject.CompareTag("coder")&&!flag)
		{
			coder = other.gameObject;
			delay = 10;
			flag = true;
		}
		else if (other.gameObject.CompareTag("clearShoot"))
		{
			bug.SetActive(true);
			bug.GetComponent<BugController> ().reset ();
			gameObject.SetActive(false);
		}
	}
}
