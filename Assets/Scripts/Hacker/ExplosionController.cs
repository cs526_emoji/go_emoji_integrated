﻿using UnityEngine;
using System.Collections;

public class ExplosionController : MonoBehaviour {

	private Animator anim;

	void Start () {
		anim = GetComponent<Animator> ();
	}
	

	void Update () {
		if (anim.GetCurrentAnimatorStateInfo (0).normalizedTime >= 1.0f) {
			gameObject.SetActive (false);
		}
	}

}
