﻿using UnityEngine;
using System.Collections;

public class coderController : MonoBehaviour {

	public GameObject blacks;
	public GameObject whites;
	public GameObject cpus;
	public GameObject hardwares;
	public GameObject player;
	public GameObject coder;
	public GameObject bridge;

	private Vector3[] dir;
	private Vector3 launchPoint;
	private int count;
	private int[] delay;
	private int shootCount;
	private int whiteCount;
	private int blackCount;
	private int cpuCount;
	private int hardwareCount;
	private GameObject cur;
	private int step;
	private bool pattern1Reseted;

	public GameObject hb;
	private int timer;
	private int maxhp;
	private int hp;
	private int oldhp;
	private bool healthChange;

	private bool dead;
	private int idxChild;

	void Start () {
		delay = new int[4];
		dir = new Vector3[4];
		Random.seed = System.Environment.TickCount;
		for (int i = 0; i < 4; i++) {
			dir [i] = new Vector3 (-1,0,0);
			dir [i] = Quaternion.Euler (0, 0, 15) * dir [i];
			dir [i].Normalize ();
			delay [i] = 0;
		}
		count = 130;
		shootCount = 0;
		whiteCount = 0;
		blackCount = 0;
		cpuCount = 0;
		hardwareCount = 0;
		step = -20;
		pattern1Reseted = false;

		timer = 0;
		maxhp = 160;
		hp = 160;
		oldhp = 160;
		healthChange = false;

		dead = false;
		idxChild = 0;
	}

	void Update () {
		if (healthChange) {
			if (timer>0) {
				HBFollow ();
				timer--;
				if (timer == 0) {
					hb.SetActive (false);
					healthChange = false;
				}
			} else if (hp == oldhp) {
				timer = 100;
			} else {
				updateHBFrame ();
			}
		}
		if (GlobalVariables.pause||GlobalVariables.freeze) {
			return;
		}
		if (count > 130) {
			denote ();
			count--;
		} else if (count > 0) {
			denote ();
			count--;
			Vector3 pos = transform.position;
			pos.y += 0.02f;
			if (dead) {
				pos.y -= 0.04f;
			}
			transform.position = pos;
			if (count == 0) {
				if (dead) {
					bridge.SetActive (true);
					int n = Random.Range (400, 600);
					n = (int)(n * GlobalVariables.boost);
					player.GetComponent<HackerPlayerController> ().getMoney (n);
					GlobalVariables.enemyKilled++;
					gameObject.SetActive (false);
				} else {
					launchPoint = new Vector3 (transform.position.x - 0.2f, transform.position.y - 0.8f, 0f);
					GetComponent<EdgeCollider2D> ().enabled = true;
					MusicController.GetInstance ().playMusic (8);
				}
			}
		} else {
			if (hp > 120) {
				pattern1 ();
			} else if (hp > 80) {
				pattern2 ();
			} else if (hp > 40) {
				pattern1Reset ();
				pattern1 ();
				pattern3 ();
			} else {
				pattern2 ();
				pattern3 ();
				pattern4 ();
			}
		}
	}

	void denote(){
		if (dead && count % 20 == 0) {
			transform.GetChild (idxChild).gameObject.SetActive (true);
			if (GlobalVariables.sound) {
				transform.GetChild (idxChild).GetComponent<AudioSource> ().Play ();
			}
			idxChild++;
		}
	}

	public void takeDmg(int val){
		if (dead) {
			return;
		}
		if (hp > val) {
			timer = 0;
			hp -= val;
			updateHB ();
		} else {
			die();
		}
	}

	void die(){
		dead = true;
		hb.SetActive (false);
		count = 200;
	}

	void pattern1(){
		if (delay [0] > 0) {
			delay [0]--;
			return;
		}
		if (shootCount % 14 == 0) {
			step = -step;
			dir[0] = Quaternion.Euler (0, 0, step/2) * dir[0];
			dir[0].Normalize ();
		}
		switch (shootCount % 4) {
		case 0:
			{
				pattern1Launch(blacks,blackCount++,1.3f,5,0);
				break;
			}
		case 1:
			{
				pattern1Launch(blacks,blackCount++,1.0f,20,0);
				break;
			}
		case 2:
			{
				pattern1Launch(whites,whiteCount++,1.3f,5,0);
				break;
			}
		case 3:
			{
				pattern1Launch(whites,whiteCount++,1.0f,20,0);
				break;
			}
		}
	}

	void pattern1Launch(GameObject shootSet, int shootSetCount, float speed, int wait, int idx){
		cur = shootSet.transform.GetChild (shootSetCount % shootSet.transform.childCount).gameObject;
		cur.SetActive (true);
		cur.transform.position = launchPoint;
		cur.GetComponent<Rigidbody2D> ().velocity = dir[idx] * speed;
		if (idx < 2) {
			shootCount++;
		}
		delay[idx] = wait;
		if (wait == 20) {
			dir[idx] = Quaternion.Euler (0, 0, step) * dir[idx];
			dir[idx].Normalize ();
		}
	}

	void pattern1Reset(){
		if (!pattern1Reseted) {
			dir [0] = new Vector3 (-1, 0, 0);
			dir [0] = Quaternion.Euler (0, 0, 15) * dir [0];
			dir [0].Normalize ();
			step = -20;
			shootCount = 0;
			pattern1Reseted = true;
		}
	}

	void pattern2(){
		if (delay [1] > 0) {
			delay [1]--;
			return;
		}
		dir[1] = new Vector3 (-1,0,0);
		float angle = Random.Range (25,155);
		float sp = Random.Range (1.5f,2.5f);
		dir[1] = Quaternion.Euler (0, 0, angle) * dir[1];
		dir[1].Normalize ();
		if (shootCount % 2 == 0) {
			pattern1Launch (blacks, blackCount++, sp, 5, 1);
		} else {
			pattern1Launch (whites, whiteCount++, sp, 5, 1);
		}
	}

	void pattern3(){
		if (delay [2] > 0) {
			delay [2]--;
			return;
		}
		dir[2] = player.transform.position - transform.position;
		dir[2].Normalize ();
		pattern1Launch (cpus, cpuCount++, 1.2f, 150, 2);
		dir[2] = Quaternion.Euler (0, 0, -30) * dir[2];
		dir[2].Normalize ();
		pattern1Launch (cpus, cpuCount++, 1.2f, 150, 2);
		dir[2] = Quaternion.Euler (0, 0, 60) * dir[2];
		dir[2].Normalize ();
		pattern1Launch (cpus, cpuCount++, 1.2f, 150, 2);
	}

	void pattern4(){
		if (delay [3] > 0) {
			delay [3]--;
			return;
		}
		dir[3] = new Vector3 (-1,0,0);
		float angle = Random.Range (25,155);
		float sp = Random.Range (1.5f,2.5f);
		dir[3] = Quaternion.Euler (0, 0, angle) * dir[3];
		dir[3].Normalize ();
		if (shootCount % 2 == 0) {
			pattern1Launch (hardwares, hardwareCount++, sp, 80, 3);
		}
	}

	void updateHB(){
		hb.SetActive (true);
		hb.transform.position = transform.position + Vector3.up * 0.5f;
		Vector3 oldPos = hb.transform.position;
		float dist = (1.0f - (float)hp / maxhp) * 45f * 0.01f;
		float scale = (float)hp / maxhp * 0.15f;
		hb.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.1f, 1f);
		hb.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		healthChange = true;
	}

	void updateHBFrame(){
		oldhp--;
		float dist = (1.0f - (float)oldhp / maxhp) * 45f * 0.01f;
		float scale = (float)oldhp / maxhp * 0.15f;
		hb.transform.position = transform.position + Vector3.up * 0.5f;
		Vector3 oldPos = hb.transform.position;
		hb.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.1f, 1f);
		hb.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
	}

	void HBFollow(){
		hb.transform.position = transform.position + Vector3.up * 0.5f;
	}

}

