﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class EmojiController : MonoBehaviour {

	public AudioClip cueHitSound;
	public AudioClip moneySound;
	public AudioClip emojiDie;
	public AudioClip potionSound;
	public AudioClip bulletSound;
	public AudioClip freezeSound;

	public GameObject ball;
	public GameObject bomb;
	public GameObject ui;

	private Rigidbody rb;
	private bool healthChange;
	private Vector3 oldPos;

	private int hp;
	private int oldhp;
	private int maxhp;
	private int damage;

	private int timer;
	private int controlCount;
	private int resetCount;
	private int state; //normal = 0, die = 1, win = 2
	private bool flag_getMoney;

	private int bombCount;

	private int potionCount;
	private int potionPow;
	private float freezeTime;
	private float curFreezeTime;
	private int freezeCount;
	private int freezeMaskTimer;
	private int cannonCount;
	private int cannonPow;
	private int cannonDelay;
	private int cannonIdx;
	public Camera cam;
	public GameObject yellowBalls;

	void Start ()
	{
		if (!GlobalVariables.sceneStarted) {
			MusicController.GetInstance ().playMusic (5);
			GlobalVariables.sceneStarted = true;
		}
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		GlobalVariables.pause = false;
		ui.transform.Find ("mask").gameObject.SetActive (true);
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		timer = 25;
		controlCount = 70;
		resetCount = 0;
		state = 0;
		rb = GetComponent<Rigidbody> ();
		maxhp = GlobalVariables.maxHP;
		hp = maxhp;
		oldhp = maxhp;
		damage = GlobalVariables.damage;
		healthChange = false;
		oldPos = ui.transform.Find ("hb_main").transform.position;
		flag_getMoney = false;
		bombCount = 0;

		initActiveSkills ();

		#if UNITY_IPHONE || UNITY_ANDROID
		GlobalVariables.resumeButtonPushed = false;
		#endif
	}

	void Update() {
		if (controlCount > 0) {
			controlCount--;
		}
		if (timer > 0) {
			timer--;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (timer*4*0.01f);
			if (timer == 0) {
				ui.transform.Find ("mask").gameObject.SetActive (false);
			}
		}
		if (healthChange) {
			updateHBFrame ();
		}
		if (state > 0) {
			ui.transform.Find ("mask").gameObject.SetActive (true);
			resetCount++;
			ui.transform.Find ("mask").GetComponent<CanvasRenderer> ().SetAlpha (resetCount*4*0.01f);
			if (resetCount == 25) {
				if (state == 1) {
					SceneManager.LoadScene ("stage6");
				} else {
					if (GlobalVariables.currentLevel == 4) {
						GlobalVariables.currentLevel = 5;
					}
					saveGame ();
					MusicController.GetInstance ().playMusic (0);
					SceneManager.LoadScene ("world_map");
				}
			}
		}
		if (flag_getMoney&&ui.transform.Find("moneyText").transform.localScale.x>0.5) {
			ui.transform.Find ("moneyText").transform.localScale -= new Vector3 (0.05f, 0.05f, 0.05f);
			if (ui.transform.Find ("moneyText").transform.localScale.x == 0.5f) {
				flag_getMoney = false;
			}
		}
		if (state==0&&transform.position.y < 0) {
			hp = 0;
			updateHB ();
			die ();
		}
		checkBombPos ();
		shootCannon ();
	}

	void FixedUpdate ()
	{
		if (controlCount > 0) {
			return;
		}

		#if UNITY_IPHONE || UNITY_ANDROID
		float moveHorizontal = Input.acceleration.x;
		float moveVertical = Input.acceleration.y;
		#else
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		#endif

		Vector3 movement = new Vector3 (moveVertical, 0f, -moveHorizontal);
		Vector3 resistance = -rb.velocity;
		resistance.Normalize ();
		rb.AddForce (resistance * rb.velocity.magnitude * 5f);
		rb.AddForce (movement * 60f);
	}

	void LateUpdate (){
		if (GlobalVariables.freeze) {
			curFreezeTime += Time.deltaTime;
			if (curFreezeTime > freezeTime) {
				GlobalVariables.freeze = false;
				freezeMaskTimer = 50;
			}
		}
		if (freezeMaskTimer > 0) {
			freezeMaskTimer--;
			ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (freezeMaskTimer*0.006f);
			if (freezeMaskTimer == 0) {
				ui.transform.Find ("freezeMask").gameObject.SetActive (false);
			}
		}
		if (GlobalVariables.startFadeIn) {
			MusicController.GetInstance ().fadeIn (0.04f);
		}
		if (GlobalVariables.startFadeOut) {
			MusicController.GetInstance ().fadeOut (0.04f);
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.CompareTag("PocketsCollider")) {
			hp = 0;
			updateHB ();
			die ();
		}
		if (collision.gameObject.CompareTag("ball")&&!GlobalVariables.freeze) {
			int damage = (int)(collision.relativeVelocity.magnitude);
			if (damage>10) {
				GlobalVariables.damageTaken += damage;
				if (hp > damage) {
					hp -= damage;
					updateHB ();
					if (GlobalVariables.sound) {
						GetComponent<AudioSource> ().Play ();
					}
				} else {
					hp = 0;
					updateHB ();
					die ();
				}
			}
		}
	}

	void die(){
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (emojiDie, 1.0f);
		}
		GlobalVariables.deathCount++;
		state = 1;
	}

	void win(){
		MusicController.GetInstance ().musicOff ();
		GlobalVariables.winCount++;
		state = 2;
	}

	void updateHB(){
		float dist = (1.0f - (float)hp / maxhp) * 60f * (Screen.width / 800.0f);
		float scale = (float)hp / maxhp * 0.2f;
		ui.transform.Find ("hb_main").transform.localScale = new Vector3 (scale, 0.2f, 1f);
		ui.transform.Find ("hb_main").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		if (oldhp > hp) {
			healthChange = true;
		} else if (oldhp < hp) {
			oldhp = hp;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	void updateHBFrame(){
		if (oldhp == hp) {
			healthChange = false;
		} else {
			oldhp--;
			float dist = (1.0f - (float)oldhp / maxhp) * 60f * (Screen.width / 800.0f);
			float scale = (float)oldhp / maxhp * 0.2f;
			ui.transform.Find ("hb_temp").transform.localScale = new Vector3 (scale, 0.2f, 1f);
			ui.transform.Find ("hb_temp").transform.position = new Vector3 (oldPos.x-dist, oldPos.y, oldPos.z);
		}
	}

	public void getMoney(int val){
		GlobalVariables.moneyEarned += val;
		GlobalVariables.money += val;
		ui.transform.Find ("moneyText").GetComponent<Text> ().text = GlobalVariables.money.ToString();
		ui.transform.Find ("moneyText").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		flag_getMoney = true;
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (moneySound, 1.0f);
		}
	}

	public void getBombIn(){
		bombCount++;
		ui.transform.Find ("flagText").GetComponent<Text> ().text = bombCount.ToString() + " / 6";
		if (bombCount == 6) {
			win ();
		}
	}

	void checkBombPos(){
		float bombX = bomb.transform.position.x;
		float bombZ = bomb.transform.position.z;
		float emojiX = transform.position.x;
		float emojiZ = transform.position.z;
		if (bombX - emojiX > 5.6f) {
			ui.transform.Find ("arrows").Find ("top").gameObject.SetActive (true);
		} else {
			ui.transform.Find ("arrows").Find ("top").gameObject.SetActive (false);
		}
		if (emojiX - bombX > 5.6f) {
			ui.transform.Find ("arrows").Find ("bot").gameObject.SetActive (true);
		} else {
			ui.transform.Find ("arrows").Find ("bot").gameObject.SetActive (false);
		}
		if (bombZ - emojiZ > 11f) {
			ui.transform.Find ("arrows").Find ("left").gameObject.SetActive (true);
		} else {
			ui.transform.Find ("arrows").Find ("left").gameObject.SetActive (false);
		}
		if (emojiZ - bombZ > 11f) {
			ui.transform.Find ("arrows").Find ("right").gameObject.SetActive (true);
		} else {
			ui.transform.Find ("arrows").Find ("right").gameObject.SetActive (false);
		}
	}

	void initActiveSkills(){
		GlobalVariables.freeze = false;
		potionCount = GlobalVariables.shopLevels [4];
		potionPow = GlobalVariables.potionPower;
		freezeTime = GlobalVariables.shopLevels [3] * 0.8f;
		curFreezeTime = 0.0f;
		freezeCount = GlobalVariables.shopLevels [3] > 0 ? 1 : 0;
		freezeMaskTimer = 0;
		cannonCount = GlobalVariables.shopLevels [5] > 0 ? 10 + 3 * GlobalVariables.shopLevels [5] : 0;
		cannonPow = GlobalVariables.shopLevels [5] * 5;
		cannonDelay = 0;
		cannonIdx = 0;
		if (potionCount > 0) {
			ui.transform.Find ("potion").gameObject.SetActive (true);
			ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		}
		if (freezeCount > 0) {
			ui.transform.Find ("hourglass").gameObject.SetActive (true);
			ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		}
		if (cannonCount > 0) {
			ui.transform.Find ("cannon").gameObject.SetActive (true);
			ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
		}
	}

	public void usePotion(){
		if (potionCount == 0) {
			return;
		}
		potionCount--;
		ui.transform.Find ("potion").Find ("text").GetComponent<Text> ().text = potionCount.ToString ();
		hp += potionPow;
		if (hp > maxhp) {
			hp = maxhp;
		}
		updateHB ();
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (potionSound, 1.0f);
		}
	}

	public void useFreeze(){
		if (freezeCount == 0) {
			return;
		}
		freezeCount--;
		ui.transform.Find ("hourglass").Find ("text").GetComponent<Text> ().text = freezeCount.ToString ();
		ui.transform.Find ("freezeMask").gameObject.SetActive (true);
		ui.transform.Find ("freezeMask").GetComponent<CanvasRenderer> ().SetAlpha (0.3f);
		GlobalVariables.freeze = true;
		if (GlobalVariables.sound) {
			GetComponent<AudioSource> ().PlayOneShot (freezeSound, 1.0f);
		}
	}

	void shootCannon(){
		if (cannonDelay > 0) {
			cannonDelay--;
		}
		else if (!GlobalVariables.pause && cannonPow > 0 && cannonCount > 0) {
			#if UNITY_IPHONE || UNITY_ANDROID
			if (Input.touchCount > 0) {
				if(GlobalVariables.resumeButtonPushed){
					GlobalVariables.resumeButtonPushed = false;
					return;
				}
				Vector2 mp = Input.GetTouch (0).position;
				Vector3 wp = cam.ScreenToWorldPoint (new Vector3(mp.x,mp.y,0));
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.y = transform.position.y;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBall3DController>().reset();
				cur.transform.position = transform.position + dir;
				cur.GetComponent<Rigidbody>().velocity = dir * 15.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#else
			if (Input.GetButtonDown ("Fire1")) {
				Vector3 mp = Input.mousePosition;
				Vector3 wp = cam.ScreenToWorldPoint (mp);
				if (inButtonArea(wp)){
					return;
				}
				cannonCount--;
				ui.transform.Find ("cannon").Find ("text").GetComponent<Text> ().text = cannonCount.ToString ();
				wp.y = transform.position.y;
				Vector3 dir = wp - transform.position;
				dir.Normalize();
				GameObject cur = yellowBalls.transform.GetChild(cannonIdx++%10).gameObject;
				cur.SetActive(true);
				cur.GetComponent<YellowBall3DController>().reset();
				cur.transform.position = transform.position + dir;
				cur.GetComponent<Rigidbody>().velocity = dir * 15.0f;
				cannonDelay = 15;
				if (GlobalVariables.sound) {
					GetComponent<AudioSource> ().PlayOneShot (bulletSound, 1.0f);
				}
			}
			#endif
		}
	}

	bool inButtonArea(Vector3 worldPoint){
		RectTransform CanvasRect = ui.GetComponent<RectTransform>();
		float canvasWidth = CanvasRect.sizeDelta.x;
		float canvasHeight = CanvasRect.sizeDelta.y;
		Vector3 viewPoint = cam.WorldToViewportPoint (worldPoint);
		Vector2 canvasPoint = new Vector2 (viewPoint.x * canvasWidth - canvasWidth * 0.5f, viewPoint.y * canvasHeight - canvasHeight * 0.5f);
		float dist;

		if (potionPow > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(100f,190f));
			if (dist < 25f)
				return true;
		}

		if (freezeTime > 0) {
			dist = Vector2.Distance (canvasPoint,new Vector2(160f,190f));
			if (dist < 25f)
				return true;
		}

		dist = Vector2.Distance (canvasPoint,new Vector2(220f,190f));
		if (dist < 25f)
			return true;
		return false;

	}

	void saveGame(){
		SaveLoad sl = new SaveLoad ();
		sl.save ();
	}
}