﻿using UnityEngine;
using System.Collections;

public class PoolGameController : MonoBehaviour {

	public GameObject cue;
	public GameObject cueBall;
	public GameObject redBalls;
	public GameObject emoji;
	public int activeRedBalls;

	public float maxForce;
	public float minForce;
	public Vector3 strikeDirection;

	public const float MIN_DISTANCE = 27.5f;
	public const float MAX_DISTANCE = 32f;
	
	public IGameObjectState currentState;

	private int delay;

	// This is kinda hacky but works
	static public PoolGameController GameInstance {
		get;
		private set;
	}

	void Start() {
		delay = 100;
		strikeDirection = Vector3.forward;

		GameInstance = this;

		currentState = new GameStates.WaitingForStrikeState(this);
	}
	
	void Update() {
		if (delay == 0) {
			currentState.Update ();
		} else {
			delay--;
		}
	}

	void FixedUpdate() {
		if (delay == 0) {
			currentState.FixedUpdate ();
		}
	}

	void LateUpdate() {
		if (delay == 0) {
			currentState.LateUpdate ();
		}
	}
		
}
