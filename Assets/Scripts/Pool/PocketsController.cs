﻿using UnityEngine;
using System.Collections;

public class PocketsController : MonoBehaviour {
	public GameObject redBalls;
	public GameObject cueBall;
	public GameObject bomb;
	public GameObject emoji;
	public GameObject poolGame;
	private PoolGameController poolGameController;

	private Vector3 originalCueBallPosition;
	private int count = 0;

	void Start() {
		Random.seed = System.Environment.TickCount;
		poolGameController = poolGame.GetComponent<PoolGameController> ();
		originalCueBallPosition = cueBall.transform.position;
	}

	void Update(){
		if (bomb.transform.position.y < -5) {
			randomSetBall (1);
		}
	}

	void OnCollisionEnter(Collision collision) {
		foreach (var transform in redBalls.GetComponentsInChildren<Transform>()) {
			if (transform.name == collision.gameObject.name) {
				GameObject.Destroy(collision.gameObject);
				poolGameController.activeRedBalls--;

				int n = UnityEngine.Random.Range (25,35);
				n = (int)(n * GlobalVariables.boost);
				emoji.GetComponent<EmojiController> ().getMoney (n);
			}
		}

		if (cueBall.transform.name == collision.gameObject.name) {
			cueBall.transform.position = originalCueBallPosition;
			int n = 100;
			n = (int)(n * GlobalVariables.boost);
			emoji.GetComponent<EmojiController> ().getMoney (n);
		}

		if (bomb.transform.name == collision.gameObject.name) {
			if (count < 5) {
				for (int i = 0; i < count + 2; i++) {
					randomSetBall (0);
				}
				randomSetBall (1);
			} else {
				bomb.SetActive (false);
			}
			count++;
			emoji.GetComponent<EmojiController> ().getBombIn ();
			int n = UnityEngine.Random.Range (25,35);
			n = (int)(n * GlobalVariables.boost);
			emoji.GetComponent<EmojiController> ().getMoney (n);
		}

	}

	void randomSetBall(int mode){
		float xPos = Random.Range (-8f, 8f);
		float zPos = Random.Range (-15f, 15f);
		if (mode == 0) {
			redBalls.transform.GetChild (poolGameController.activeRedBalls).gameObject.SetActive (true);
			redBalls.transform.GetChild (poolGameController.activeRedBalls).gameObject.transform.position = new Vector3 (xPos, 0.69f, zPos);
			poolGameController.activeRedBalls++;
		} else {
			bomb.transform.position = new Vector3 (xPos, 0.69f, zPos);
		}
	}
		
}
