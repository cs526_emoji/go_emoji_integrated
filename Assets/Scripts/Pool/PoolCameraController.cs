﻿using UnityEngine;
using System.Collections;

public class PoolCameraController : MonoBehaviour {

	public GameObject player; 

	private float offsetX;
	private float offsetZ;
	private float minX;
	private float minZ;

	private Camera cam;
	private int timer;
	private Vector3 pos;

	void Start ()
	{
		cam = GetComponent<Camera> ();
		timer = 70;
		minX = -5.6f;
		minZ = -11f;
	}

	void LateUpdate ()
	{
		if (timer > 45) {
			timer--;
		} else if (timer > 0) {
			pos = transform.position;
			pos.z -= 0.2f;
			cam.orthographicSize -= 0.2f;
			transform.position = pos;
			timer--;
		} else if (timer == 0) {
			offsetX = (transform.position.x - player.transform.position.x) / 2;
			offsetZ = (transform.position.z - player.transform.position.z) / 2;
			timer--;
		} else {
			pos = transform.position;
			if (transform.position.x - player.transform.position.x > offsetX) {
				pos.x = player.transform.position.x + offsetX;
			} else if (transform.position.x - player.transform.position.x < -offsetX) {
				pos.x = player.transform.position.x - offsetX;
			}
			if (transform.position.z - player.transform.position.z > offsetZ) {
				pos.z = player.transform.position.z + offsetZ;
			} else if (transform.position.z - player.transform.position.z < -offsetZ) {
				pos.z = player.transform.position.z - offsetZ;
			}
			if (pos.x < minX) {
				pos.x = minX;
			} else if (pos.x > -minX) {
				pos.x = -minX;
			}
			if (pos.z < minZ) {
				pos.z = minZ;
			} else if (pos.z > -minZ) {
				pos.z = -minZ;
			}
			transform.position = pos;
		}
	}

}
