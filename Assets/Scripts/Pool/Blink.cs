﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour {

	private int count;

	void Start () {
		count = 20;
	}

	void Update () {
		if (GlobalVariables.pause) {
			return;
		}
		if (count > 0) {
			transform.GetComponent<CanvasRenderer> ().SetAlpha (0.05f * count);
			count--;
		} else if (count > -19) {
			transform.GetComponent<CanvasRenderer> ().SetAlpha (-0.05f * count);
			count--;
		} else {
			transform.GetComponent<CanvasRenderer> ().SetAlpha (-0.05f * count);
			count = 20;
		}
	}
}
