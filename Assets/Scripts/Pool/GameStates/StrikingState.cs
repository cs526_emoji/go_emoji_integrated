﻿using UnityEngine;
using System.Collections;

namespace GameStates {
	public class StrikingState : AbstractGameObjectState {
		private PoolGameController gameController;

		private GameObject cue;
		private GameObject cueBall;

		private float cueDirection = -1;
		private float speed = 7;

		private int timer = 0;

		private Vector3 offset;

		public StrikingState(MonoBehaviour parent) : base(parent) { 
			gameController = (PoolGameController)parent;
			cue = gameController.cue;
			cueBall = gameController.cueBall;

			offset = cue.transform.position - cueBall.transform.position;
		}

		public override void Update() {
			if (GlobalVariables.freeze) {
				return;
			}
			if (timer < 20) {
				timer++;
				if (timer == 20) {
					timer = 0;
					gameController.currentState = new GameStates.StrikeState(gameController);
				}
			}

//			if (Input.GetButtonUp("Fire1")) {
//				gameController.currentState = new GameStates.StrikeState(gameController);
//			}
		}

		public override void FixedUpdate () {
			if (GlobalVariables.freeze) {
				return;
			}
			var distance = Vector3.Distance(cue.transform.position, cueBall.transform.position);
			if (distance < PoolGameController.MIN_DISTANCE || distance > PoolGameController.MAX_DISTANCE)
				cueDirection *= -1;
			cue.transform.position = cueBall.transform.position + offset;
			cue.transform.Translate(Vector3.down * speed * cueDirection * Time.fixedDeltaTime);
			offset = cue.transform.position - cueBall.transform.position;
		}
	}
}