﻿using UnityEngine;
using System.Collections;

namespace GameStates {
	public class WaitingForNextTurnState : AbstractGameObjectState {
		private PoolGameController gameController;
		private GameObject cue;
		private GameObject cueBall;

		private Vector3 cameraOffset;
		private Vector3 cueOffset;
		private Quaternion cameraRotation;
		private Quaternion cueRotation;

		public WaitingForNextTurnState(MonoBehaviour parent) : base(parent) {
			gameController = (PoolGameController)parent;

			cue = gameController.cue;
			cueBall = gameController.cueBall;

			cueOffset = cueBall.transform.position - cue.transform.position;
			cueRotation = cue.transform.rotation;
		}

		public override void FixedUpdate() {
			if (GlobalVariables.freeze) {
				return;
			}
			if (cueBall.transform.position.y < -5) {
				cueBall.transform.position = new Vector3 (0f,0.5f,-13.7f);
			}

			var cueBallBody = cueBall.GetComponent<Rigidbody>();
			if (!(cueBallBody.IsSleeping() || cueBallBody.velocity == Vector3.zero))
				return;

			// If all balls are sleeping, time for the next turn
			// This is kinda hacky but gets the job done
			gameController.currentState = new WaitingForStrikeState(gameController);

		}

		public override void LateUpdate() {
			if (GlobalVariables.freeze) {
				return;
			}
			cue.transform.position = cueBall.transform.position - cueOffset;
			cue.transform.rotation = cueRotation;
		}
	}
}