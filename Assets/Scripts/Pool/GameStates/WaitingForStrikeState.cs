﻿using UnityEngine;
using System.Collections;

namespace GameStates {

	public class WaitingForStrikeState  : AbstractGameObjectState {
		private GameObject cue;
		private GameObject cueBall;
		private GameObject redBalls;
		private GameObject targetBall;
		private GameObject emoji;
		private Vector3 targetDir;
		private Vector3 currentDir;
		private float step;
		private int timer;
		private PoolGameController gameController;

		private Vector3 offset;

		public WaitingForStrikeState(MonoBehaviour parent) : base(parent) { 
			gameController = (PoolGameController)parent;
			cue = gameController.cue;
			cueBall = gameController.cueBall;
			emoji = gameController.emoji;
			offset = cue.transform.position - cueBall.transform.position;

			timer = 0;
			redBalls = gameController.redBalls;

			Random.seed = System.Environment.TickCount;
			int n = Random.Range (0, gameController.activeRedBalls*2);
			if (n >= gameController.activeRedBalls) {
				targetBall = emoji;
			} else {
				targetBall = redBalls.transform.GetChild (n).gameObject;
			}
			targetDir = targetBall.transform.position - cueBall.transform.position;
			currentDir = gameController.strikeDirection;
			float angle = Vector3.Angle(currentDir,targetDir);
			Vector3 cross = Vector3.Cross (currentDir, targetDir);
			if (cross.y < 0) {
				angle = -angle;
			}
			step = angle / 20f;

			cue.GetComponent<Renderer>().enabled = true;
		}

		public override void Update() {
			if (GlobalVariables.freeze) {
				return;
			}
			if (timer < 20) {
				cue.transform.position = cueBall.transform.position + offset;
				cue.transform.RotateAround(cueBall.transform.position, Vector3.up, step);
				offset = cue.transform.position - cueBall.transform.position;
				timer++;
				if (timer == 20) {
					//redBalls.transform.GetChild (0).gameObject.SetActive (true);
					timer = 0;
					gameController.strikeDirection = targetDir;
					gameController.strikeDirection.Normalize ();
					gameController.currentState = new GameStates.StrikingState(gameController);
				}
			}
//			gameController.strikeDirection = emoji.transform.position - cueBall.transform.position;
//			float angle = Mathf.Atan2(gameController.strikeDirection.y, gameController.strikeDirection.x) * Mathf.Rad2Deg + 90;
//			Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
//			cue.transform.RotateAround(cueBall.transform.position, Vector3.up, angle);

//			var x = Input.GetAxis("Horizontal");
//			
//			if (x != 0) {
//				var angle = x * 75 * Time.deltaTime;
//				gameController.strikeDirection = Quaternion.AngleAxis(angle, Vector3.up) * gameController.strikeDirection;
//				//mainCamera.transform.RotateAround(cueBall.transform.position, Vector3.up, angle);
//				cue.transform.RotateAround(cueBall.transform.position, Vector3.up, angle);
//			}
			//Debug.DrawLine(cueBall.transform.position, cueBall.transform.position + gameController.strikeDirection * 10);

//			if (Input.GetButtonDown("Fire1")) {
//				gameController.currentState = new GameStates.StrikingState(gameController);
//			}
		}
	}
}